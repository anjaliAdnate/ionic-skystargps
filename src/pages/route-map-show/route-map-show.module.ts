import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RouteMapShowPage } from './route-map-show';

@NgModule({
  declarations: [
    RouteMapShowPage,
  ],
  imports: [
    IonicPageModule.forChild(RouteMapShowPage),
  ],
})
export class RouteMapShowPageModule {}
