import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, Events } from "ionic-angular";
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-customers',
    templateUrl: './dealers.html'
})

export class DealerPage {

    islogin: any = {};
    page: number = 1;
    limit: number = 5;
    DealerArraySearch: any = [];
    DealerArray: any = [];
    ndata: any = [];
    DealerData: any = [];
    CratedeOn: string;
    expirydate: string;
    time: string;
    date: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiCall: ApiServiceProvider,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public events: Events
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("_id=> " + this.islogin._id);
        // console.log("islogin devices => " + JSON.stringify(this.islogin));
        // this.setsmsforotp = localStorage.getItem('setsms');
        // this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        // console.log("isSuperAdminStatus=> " + this.isSuperAdminStatus);
        // this.isDealer = this.islogin.isDealer
        // console.log("isDealer=> " + this.isDealer);
    }

    ionViewDidLoad() {
        this.getDealersListCall();
    }

    getDealersListCall() {
        this.apiCall.startLoading().present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this.DealerArray = data;
                this.DealerArraySearch = data;
                console.log("Dealer data=> ", data);
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log("getting error from server=> ", err);
                    var s = JSON.parse(err._body);
                    var p = s.message;
                    let toast = this.toastCtrl.create({
                        message: "No Dealer(s) found",
                        duration: 2000,
                        position: "bottom"
                    })

                    toast.present();

                    toast.onDidDismiss(() => {
                        this.navCtrl.setRoot("DashboardPage");
                    })
                });
    }

    addDealersModal() {
        let modal = this.modalCtrl.create('AddDealerPage');
        modal.onDidDismiss(() => {
            console.log("modal dismissed!")
            this.getDealersListCall();
        })
        modal.present();
    }

    _editDealer(item) {
        let modal = this.modalCtrl.create('EditDealerPage', {
            param: item
        });
        modal.onDidDismiss(() => {
            console.log("modal dismissed!")
            this.getDealersListCall();
        })
        modal.present();
    }

    DeleteDealer(_id) {
        let alert = this.alerCtrl.create({
            message: 'Do you want to delete this Customer?',
            buttons: [{
                text: 'No'
            },
            {
                text: 'YES',
                handler: () => {
                    this.deleteDeal(_id);
                }
            }]
        });
        alert.present();
    }

    deleteDeal(_id) {
        console.log('user id bole to dealer', _id)

        var deletePayload = {
            'userId': _id,
            'deleteuser': true
        }
        this.apiCall.startLoading().present();
        this.apiCall.deleteDealerCall(deletePayload).
            subscribe(data => {
                this.apiCall.stopLoading();
                console.log("deleted dealer data=> " + data)
                let toast = this.toastCtrl.create({
                    message: 'Deleted dealer successfully.',
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    console.log('Dismissed toast');
                    this.getDealersListCall();
                });

                toast.present();
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log(err);
                });
    }

    callSearch(ev) {
        console.log(ev.target.value)
        var searchKey = ev.target.value;
        this.apiCall.DealerSearchService(this.islogin._id, this.page, this.limit, searchKey)
            .subscribe(data => {
                this.DealerArraySearch = data;
                this.DealerArray = data;
            }, err => {
                console.log("error dealer=> ", JSON.stringify(err._body))
                var a = JSON.parse(err._body);
                var b = a.message;
                let toast = this.toastCtrl.create({
                    message: "No Dealer found for search key '"+ev.target.value+"' ..",
                    duration: 3000,
                    position: "bottom"
                })

                toast.present();

                toast.onDidDismiss(() => {
                    ev.target.value = '';
                    this.getDealersListCall();
                })
            })
    }

    onClear(ev) {
        // debugger;
        this.getDealersListCall();
        ev.target.value = '';
    }

    switchDealer(_id) {
        //debugger;
        console.log(_id)
        // localStorage.setItem('isDealervalue', 'true');
        // localStorage.setItem('isSuperAdminValue', 'false');
        // $rootScope.dealer = $rootScope.islogin;
        localStorage.setItem('superAdminData', JSON.stringify(this.islogin));

        localStorage.setItem('custumer_status', 'OFF');
        localStorage.setItem('dealer_status', 'ON');

        this.apiCall.getcustToken(_id)
            .subscribe(res => {
                console.log('UserChangeObj=>', res)
                var custToken = res;

                var logindata = JSON.stringify(custToken);
                var logindetails = JSON.parse(logindata);
                var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
                // console.log('token=>', logindata);

                var details = JSON.parse(userDetails);
                // console.log(details.isDealer);
                localStorage.setItem("loginflag", "loginflag");
                localStorage.setItem('details', JSON.stringify(details));

                var dealerSwitchObj = {
                    "logindata": logindata,
                    "details": userDetails,
                    'condition_chk': details.isDealer
                }

                var temp = localStorage.getItem('isDealervalue');
                console.log("temp=> ", temp);
                this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
                this.events.publish("sidemenu:event", temp);
                this.navCtrl.setRoot('DashboardPage');

            }, err => {
                console.log(err);
            })
    }

    dealerStatus(data) {
        // console.log("status=> " + Customersdeta.status)
        // console.log(Customersdeta._id)
        // console.log(this.islogin._id);
        var msg;
        if (data.status) {
            msg = 'Do you want to Deactivate this Dealer?'
        } else {
            msg = 'Do you want to Activate this Dealer?'
        }
        let alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                text: 'YES',
                handler: () => {
                    this.user_status(data);
                }
            },
            {
                text: 'NO',
                handler: () => {
                    this.getDealersListCall();
                }
            }]
        });
        alert.present();
    }

    user_status(data) {
        var stat;
        if (data.status) {
            stat = false;
        } else {
            stat = true;
        }

        var ddata = {
            "uId": data._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(ddata)
            .subscribe(data => {
                this.apiCall.stopLoading();
                // this.DeletedDevice = data;
                console.log("Dealer data=> ", data)
                // console.log("DeletedDevice=> " + this.DeletedDevice)
                let toast = this.toastCtrl.create({
                    message: 'Dealer status updated successfully!',
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    console.log('Dismissed toast');
                    this.getDealersListCall();
                });

                toast.present();
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log("error => ", err)
                    // var body = err._body;
                    // var msg = JSON.parse(body);
                    // let alert = this.alerCtrl.create({
                    //   title: 'Oops!',
                    //   message: msg.message,
                    //   buttons: ['OK']
                    // });
                    // alert.present();
                });
    }

    doInfinite(infiniteScroll) {
        let that = this;
        that.page = that.page + 1;

        setTimeout(() => {
            //   var baseURLp;
            //   baseURLp = 'https://www.oneqlik.in/users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit;
            that.ndata = [];
            this.apiCall.getDealersCall(that.islogin._id, that.page, that.limit)
                .subscribe(data => {
                    that.ndata = data;

                    for (let i = 0; i < that.ndata.length; i++) {
                        that.DealerData.push(that.ndata[i]);
                    }
                    this.DealerArray = [];

                    for (var i = 0; i < this.DealerData.length; i++) {
                        this.CratedeOn = JSON.stringify(this.DealerData[i].created_on).split('"')[1].split('T')[0];
                        var gmtDateTime = moment.utc(JSON.stringify(this.DealerData[i].created_on).split('T')[1].split('.')[0], "HH:mm:ss");
                        var gmtDate = moment.utc(JSON.stringify(this.DealerData[i].created_on).slice(0, -1).split('T'), "YYYY-MM-DD");

                        if (this.DealerData[i].expiration_date != null) {
                            // var expirationDate = JSON.stringify(this.DealerData[i].expiration_date).split('"')[1].split('T')[0];
                            // var gmtDateTime1 = moment.utc(JSON.stringify(this.DealerData[i].expiration_date).split('T')[1].split('.')[0], "HH:mm:ss");
                            var gmtDate2 = moment.utc(JSON.stringify(this.DealerData[i].expiration_date).slice(0, -1).split('T'), "YYYY-MM-DD");
                            this.expirydate = gmtDate2.format('DD/MM/YYYY');
                        } else {
                            this.expirydate = null;
                        }

                        this.time = gmtDateTime.local().format(' h:mm:ss a');
                        this.date = gmtDate.format('DD/MM/YYYY');

                        this.DealerArray.push({
                            'dealer_id': this.DealerData[i].dealer_id,
                            'dealer_firstname': this.DealerData[i].dealer_firstname,
                            'dealer_lastname': this.DealerData[i].dealer_lastname,
                            'email': this.DealerData[i].email,
                            'phone': this.DealerData[i].phone,
                            'created_on': this.DealerData[i].created_on,
                            'status': this.DealerData[i].status,
                            // 'pass': this.DealerData[i].pass,
                            'vehicleCount': this.DealerData[i].vehicleCount,
                            // 'userid': this.DealerData[i].userid,
                            // 'address': this.DealerData[i].address,
                            // 'dealer_firstname': this.DealerData[i].dealer_firstname,
                            // 'expiration_date': this.expirydate
                        });

                    }
                    this.DealerArraySearch = this.DealerArray;

                },
                    err => {
                        this.apiCall.stopLoading();
                        console.log("error found=> " + err);
                    });

            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    }
}