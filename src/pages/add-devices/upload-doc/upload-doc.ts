import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ActionSheetController, Platform, ToastController, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-upload-doc',
  templateUrl: 'upload-doc.html',
})
export class UploadDocPage {
  docType: any;
  docImge: boolean;
  lastImage: string = null;
  Imgloading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private file: File,
    public platform: Platform,
    private filePath: FilePath,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private transfer: Transfer,
    public transferObj: TransferObject,
    public apiCall: ApiServiceProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadDocPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSelectChange(selectedValue: any) {
    console.log('Selected', selectedValue);
    this.docType = selectedValue;
  }

  brows() {
    this.docImge = true;
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then((imagePath) => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      // this.presentToast('Error while selecting image.');
      console.log("Error while selecting image.", err);
    });
  }

  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  public pathForImage(img) {
    console.log("Image=>", img);
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  public uploadImage() {
    var url = "https://www.oneqlik.in/users/uploadImage";
    var targetPath = this.pathForImage(this.lastImage);
    var filename = this.lastImage;
    var options = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg",
      params: { 'fileName': filename }
    };
    this.transferObj = this.transfer.create();
    this.Imgloading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.Imgloading.present();
    this.transferObj.upload(targetPath, url, options).then(data => {
      this.Imgloading.dismissAll();
      this.dlUpdate(data.response);
    }, err => {
      console.log("uploadError=>", err)
      this.lastImage = null;
      this.Imgloading.dismissAll();
      this.presentToast('Error while uploading file, Please try again !!!');
    });
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  dlUpdate(dllink) {
    // var dlObj = {
    //   image_path: dllink,
    //   phone: this.mobilenumber,
    //   img_type: this.docString
    // }

    var dlObj = {
      image_path: dllink,
      phone: "7507500582",
      img_type: "poc"
    }
    this.apiCall.startLoading();
    this.apiCall.updateDL(dlObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        this.presentToast('Image succesful uploaded.');
        console.log("returned obj: ", res)
        this.lastImage = null;
        this.docImge = undefined;
        // this.docString = undefined;
        // this.placeString = "upload another document"
        // this.showButton = true;
        // this.navCtrl.setRoot("LoginPage");
      }, err => {
        this.apiCall.stopLoading();
        this.presentToast('Internal server Error !!!');
      })
  }

}
