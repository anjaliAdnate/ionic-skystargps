webpackJsonp([41],{

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDealerPageModule", function() { return AddDealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_dealer__ = __webpack_require__(943);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddDealerPageModule = /** @class */ (function () {
    function AddDealerPageModule() {
    }
    AddDealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_dealer__["a" /* AddDealerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_dealer__["a" /* AddDealerPage */]),
            ],
        })
    ], AddDealerPageModule);
    return AddDealerPageModule;
}());

//# sourceMappingURL=add-dealer.module.js.map

/***/ }),

/***/ 943:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddDealerPage = /** @class */ (function () {
    function AddDealerPage(navCtrl, navParams, formBuilder, apicall, alerCtrl, viewCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apicall = apicall;
        this.alerCtrl = alerCtrl;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.addDealerform = formBuilder.group({
            userId: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            Firstname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            LastName: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            emailid: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            contact_num: [''],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            cpassword: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            address: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    AddDealerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddDealerPage');
    };
    AddDealerPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddDealerPage.prototype._submit = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.addDealerform.valid) {
            // if (this.islogin.role == 'supAdm') {  
            // only superadmin can add the dealer, thats why we dont need to check condition if it is superadmin or dealer
            var payload = {
                address: this.addDealerform.value.address,
                custumer: false,
                email: this.addDealerform.value.emailid,
                first_name: this.addDealerform.value.Firstname,
                // imageDoc: [{ doctype: "", image: "" }],
                isDealer: this.islogin.isDealer,
                last_name: this.addDealerform.value.LastName,
                org_name: this.islogin._orgName,
                password: this.addDealerform.value.password,
                supAdmin: this.islogin._id,
                sysadmin: this.islogin.isSuperAdmin,
                user_id: this.addDealerform.value.userId
            };
            this.apicall.startLoading().present();
            this.apicall.signupApi(payload)
                .subscribe(function (data) {
                _this.apicall.stopLoading();
                console.log("dealer added data=> ", data);
                var toast = _this.toastCtrl.create({
                    message: 'Dealer added successfully!',
                    position: 'top',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss();
                });
                toast.present();
            }, function (err) {
                _this.apicall.stopLoading();
                console.log("error occured=> ", err);
                var body = err._body;
                var msg = JSON.parse(body);
                var namepass = [];
                namepass = msg.split(":");
                var name = namepass[1];
                var alert = _this.alerCtrl.create({
                    // title: 'Oops!',
                    message: name,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    AddDealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-customer-model',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\dealers\add-dealer\add-dealer.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Add Dealer</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n<ion-content>\n  <form [formGroup]="addDealerform">\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">User ID*</ion-label>\n      <ion-input formControlName="userId" type="text"></ion-input>\n    </ion-item>\n\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.userId.valid && (addDealerform.controls.userId.dirty || submitAttempt)">\n      <p>Please enter User ID</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">First Name*</ion-label>\n      <ion-input formControlName="Firstname" type="text"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.Firstname.valid && (addDealerform.controls.Firstname.dirty || submitAttempt)">\n      <p>Please enter First Name</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">Last Name*</ion-label>\n      <ion-input formControlName="LastName" type="text"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.LastName.valid && (addDealerform.controls.LastName.dirty || submitAttempt)">\n      <p>Please enter Last Name</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">Email ID</ion-label>\n      <ion-input formControlName="emailid" type="email"></ion-input>\n    </ion-item>\n    <!-- <ion-item class="logitem1" *ngIf="!addDealerform.controls.emailid.valid && (addDealerform.controls.emailid.dirty || submitAttempt)">\n          <p>Please enter User ID</p>\n      </ion-item> -->\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">Mobile Number</ion-label>\n      <ion-input formControlName="contact_num" type="number" maxlength="10" minlength="10"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.contact_num.valid && (addDealerform.controls.contact_num.dirty || submitAttempt)">\n      <p>mobile number required and should be 10 digits!</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">Password*</ion-label>\n      <ion-input formControlName="password" type="password"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.password.valid && (addDealerform.controls.password.dirty || submitAttempt)">\n      <p>Invalid Password</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">Confirm Password*</ion-label>\n      <ion-input formControlName="cpassword" type="password"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.cpassword.valid && (addDealerform.controls.cpassword.dirty || submitAttempt)">\n      <p>Please enter Confirm Password</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">Address*</ion-label>\n      <ion-input formControlName="address" type="text"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.address.valid && (addDealerform.controls.address.dirty || submitAttempt)">\n      <p>Address required!</p>\n    </ion-item>\n  </form>\n</ion-content>\n<ion-footer class="footSty">\n  <ion-toolbar>\n    <ion-row no-padding>\n      <ion-col width-50 style="text-align: center;">\n        <button ion-button clear color="light" (click)="_submit()">SUBMIT</button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\dealers\add-dealer\add-dealer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], AddDealerPage);
    return AddDealerPage;
}());

//# sourceMappingURL=add-dealer.js.map

/***/ })

});
//# sourceMappingURL=41.js.map