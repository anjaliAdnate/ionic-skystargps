webpackJsonp([46],{

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaytmwalletloginPageModule", function() { return PaytmwalletloginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__ = __webpack_require__(662);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaytmwalletloginPageModule = /** @class */ (function () {
    function PaytmwalletloginPageModule() {
    }
    PaytmwalletloginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */]),
            ],
        })
    ], PaytmwalletloginPageModule);
    return PaytmwalletloginPageModule;
}());

//# sourceMappingURL=paytmwalletlogin.module.js.map

/***/ }),

/***/ 662:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaytmwalletloginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { WalletPage } from '../wallet/wallet';

var PaytmwalletloginPage = /** @class */ (function () {
    function PaytmwalletloginPage(navCtrl, navParams, toastCtrl, apiCall, menu, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.menu = menu;
        this.toast = toast;
        this.successresponse = false;
        this.inputform = false;
        this.navOptions = {
            animation: 'ios-transition'
        };
        // debugger
        var num = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
        this.paytmnumber = num.phn;
    }
    PaytmwalletloginPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(true);
    };
    PaytmwalletloginPage.prototype.paytmwallet = function () {
        var _this = this;
        var useDetails = localStorage.getItem('details');
        var userId = JSON.parse(useDetails)._id;
        // payment gateway integration function
        var paytmUserCredentials = {
            "user": userId,
            "app_id": "OneQlikVTS",
            "phone": this.paytmnumber,
            "scope": "wallet",
            "responseType": "token",
        };
        this.apiCall.paytmLoginWallet(paytmUserCredentials)
            .subscribe(function (res) {
            _this.successresponse = true;
            _this.inputform = true;
            _this.paytmSignupOTPResponse = res;
            var errToast_success = _this.toast.create({
                message: 'OTP sent successfully, Please type OTP in next field !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            errToast_success.present();
        }, function (err) {
            var errToast = _this.toast.create({
                message: 'Server error , Please try after somtime !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            if (err) {
                console.log("error occured !!!");
                errToast.present();
            }
        });
    };
    PaytmwalletloginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PaytmwalletloginPage.prototype.resndOTP = function () {
        var _this = this;
        var useDetails = localStorage.getItem('details');
        var userId = JSON.parse(useDetails)._id;
        var paytmUserCredentials = {
            "user": userId,
            "app_id": "OneQlikVTS",
            "phone": this.paytmnumber,
            "scope": "wallet",
            "responseType": "token"
        };
        this.apiCall.paytmLoginWallet(paytmUserCredentials)
            .subscribe(function (res) {
            _this.successresponse = true;
            _this.inputform = true;
            _this.paytmSignupOTPResponse = res;
            var errToast_success = _this.toast.create({
                message: 'OTP sent successfully, Please type OTP in next field !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            errToast_success.present();
        }, function (err) {
            var errToast = _this.toast.create({
                message: 'Server error , Please try after somtime !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            if (err) {
                errToast.present();
            }
        });
        //  payment gateway integration function
    };
    PaytmwalletloginPage.prototype.paytmAuthantication = function () {
        var _this = this;
        this.apiCall.startLoading();
        var otpCredentials = {
            "otp": this.paytmotp,
            "transac_id": this.paytmSignupOTPResponse.transac_id,
            "app_id": "OneQlikVTS"
        };
        this.apiCall.paytmOTPvalidation(otpCredentials)
            .subscribe(function (res) {
            console.log("response from wallet page=> ", res);
            _this.apiCall.stopLoading();
            // debugger;
            var s = JSON.parse(res.response);
            if (s.status == 'FAILURE') {
                var toast = _this.toastCtrl.create({
                    message: s.message,
                    duration: 2000,
                    position: "bottom"
                });
                toast.onDidDismiss(function () {
                    _this.paytmotp = undefined;
                });
                toast.present();
            }
            else {
                localStorage.setItem('paytmregNum', _this.paytmnumber);
                _this.navCtrl.push("WalletPage", null, _this.navOptions);
            }
        }, function (err) {
            console.log("error found=> " + err);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: 'internal server Error, Please try after sometime !!!',
                duration: 3000,
                position: 'top'
            });
            toast.present();
        });
    };
    PaytmwalletloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-paytmwalletlogin',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\add-devices\paytmwalletlogin\paytmwalletlogin.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Payment Method</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n\n  <!-- <div style="background:#8eda4a;">\n\n\n\n    <h5 style="margin: 0%;color: white;font-weight: 300;padding-top: 44%;text-align: center;font-size: 1.8rem;padding-bottom: 15px;">Payment Method</h5>\n\n  </div> -->\n\n  <div style="padding: 0% 10% 0% 10%;">\n\n\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">Paytm</h4>\n\n    <ion-item style="padding-left: 0px;">\n\n      <ion-input [disabled]=\'inputform\' [(ngModel)]="paytmnumber" type="number" placeholder="Mobile Number*"></ion-input>\n\n    </ion-item>\n\n    <p style="color:grey">Linking wallet is one time process. Next time, checkout will be a breeze!</p>\n\n  </div>\n\n  <button [disabled]=\'inputform\' ion-button full color="gpsc" style="height: 35px;width: 45%;margin-left: 30%;font-size: 15px;color: white;margin-top: 15px;color: rgb(255, 255, 255);"\n\n    (click)="paytmwallet()">Continue</button>\n\n  <ion-row style="padding: 2% 10% 0% 10%;" *ngIf="successresponse">\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">Enter verification code</h4>\n\n    <p style="margin-top: 0px;color: grey;margin-bottom: 0;">Paytm has send verification code on {{paytmnumber}} via sms Please enter it below, and you are done!</p>\n\n    <ion-item style="padding: 0px;">\n\n      <ion-input [(ngModel)]="paytmotp" type="number" placeholder="Enter OTP"></ion-input>\n\n    </ion-item>\n\n    <ion-col col-sm-6>\n\n      <button ion-button full style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n\n        (click)="resndOTP()">Resend</button>\n\n    </ion-col>\n\n    <ion-col col-sm-6>\n\n      <button ion-button full [disabled]="paytmotp == undefined" style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n\n        (click)="paytmAuthantication()">Proceed</button>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-content>\n\n<!-- <ion-footer *ngIf="successresponse">\n\n  <ion-row style="margin-top: 4px;width: 90%;margin-left: 5%;">\n\n    <ion-col>\n\n      <button ion-button full color="gpsc" style="margin: 0%;height: 43px;font-size: 14px;border-radius: 8px" (click)="resndOTP()">Resend</button>\n\n    </ion-col>\n\n    <ion-col>\n\n      <button ion-button full color="gpsc" style="margin: 0%;height: 43px;font-size: 14px;border-radius: 8px" (click)="paytmAuthantication()">Proceed</button>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-footer> -->'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\add-devices\paytmwalletlogin\paytmwalletlogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], PaytmwalletloginPage);
    return PaytmwalletloginPage;
}());

//# sourceMappingURL=paytmwalletlogin.js.map

/***/ })

});
//# sourceMappingURL=46.js.map