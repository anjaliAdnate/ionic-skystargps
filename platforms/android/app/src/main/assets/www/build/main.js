webpackJsonp([51],{

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-devices/add-devices.module": [
		367,
		49
	],
	"../pages/add-devices/immobilize/immobilize.module": [
		319,
		48
	],
	"../pages/add-devices/payment-greeting/payment-greeting.module": [
		320,
		47
	],
	"../pages/add-devices/payment-secure/payment-secure.module": [
		321,
		25
	],
	"../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module": [
		322,
		46
	],
	"../pages/add-devices/update-device/update-device.module": [
		354,
		21
	],
	"../pages/add-devices/upload-doc/upload-doc.module": [
		323,
		45
	],
	"../pages/add-devices/vehicle-details/vehicle-details.module": [
		364,
		4
	],
	"../pages/add-devices/wallet/wallet.module": [
		324,
		0
	],
	"../pages/all-notifications/all-notifications.module": [
		355,
		23
	],
	"../pages/contact-us/contact-us.module": [
		325,
		44
	],
	"../pages/customers/customers.module": [
		356,
		43
	],
	"../pages/customers/modals/add-customer-modal/add-customer-modal.module": [
		357,
		20
	],
	"../pages/customers/modals/add-device-modal.module": [
		358,
		19
	],
	"../pages/customers/modals/group-modal/group-modal.module": [
		326,
		42
	],
	"../pages/customers/modals/update-cust/update-cust.module": [
		327,
		18
	],
	"../pages/daily-report/daily-report.module": [
		328,
		17
	],
	"../pages/dashboard/dashboard.module": [
		359,
		1
	],
	"../pages/dealers/add-dealer/add-dealer.module": [
		329,
		41
	],
	"../pages/dealers/dealers.module": [
		360,
		16
	],
	"../pages/dealers/edit-dealer/edit-dealer.module": [
		330,
		40
	],
	"../pages/device-summary-repo/device-summary-repo.module": [
		332,
		15
	],
	"../pages/distance-report/distance-report.module": [
		331,
		14
	],
	"../pages/feedback/feedback.module": [
		333,
		24
	],
	"../pages/fuel-report/fuel-report.module": [
		334,
		13
	],
	"../pages/geofence-report/geofence-report.module": [
		335,
		12
	],
	"../pages/geofence/add-geofence/add-geofence.module": [
		365,
		26
	],
	"../pages/geofence/geofence-show/geofence-show.module": [
		336,
		39
	],
	"../pages/geofence/geofence.module": [
		337,
		38
	],
	"../pages/groups/groups.module": [
		338,
		37
	],
	"../pages/history-device/history-device.module": [
		366,
		11
	],
	"../pages/ignition-report/ignition-report.module": [
		339,
		10
	],
	"../pages/live-single-device/device-settings/device-settings.module": [
		340,
		36
	],
	"../pages/live-single-device/live-single-device.module": [
		368,
		2
	],
	"../pages/live/expired/expired.module": [
		341,
		35
	],
	"../pages/live/live.module": [
		369,
		22
	],
	"../pages/login/login.module": [
		361,
		34
	],
	"../pages/over-speed-repo/over-speed-repo.module": [
		342,
		9
	],
	"../pages/profile/profile.module": [
		362,
		50
	],
	"../pages/profile/settings/settings.module": [
		343,
		33
	],
	"../pages/route-map-show/route-map-show.module": [
		344,
		32
	],
	"../pages/route-voilations/route-voilations.module": [
		345,
		8
	],
	"../pages/route/route.module": [
		347,
		31
	],
	"../pages/show-geofence/show-geofence.module": [
		346,
		30
	],
	"../pages/show-route/show-route.module": [
		348,
		29
	],
	"../pages/show-trip/show-trip.module": [
		349,
		28
	],
	"../pages/sos-report/sos-report.module": [
		350,
		7
	],
	"../pages/speed-repo/speed-repo.module": [
		351,
		3
	],
	"../pages/stoppages-repo/stoppages-repo.module": [
		352,
		6
	],
	"../pages/trip-report/trip-report.module": [
		353,
		5
	],
	"../pages/trip-report/trip-review/trip-review.module": [
		363,
		27
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatusEnum */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum || (ConnectionStatusEnum = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(alertCtrl, network, eventCtrl) {
        this.alertCtrl = alertCtrl;
        this.network = network;
        this.eventCtrl = eventCtrl;
        this._status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        console.log('Hello NetworkProvider Provider');
        this.previousStatus = ConnectionStatusEnum.Online;
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Online) {
                _this.setStatus(ConnectionStatusEnum.Offline);
                _this.eventCtrl.publish('network:offline');
            }
            // if (this.previousStatus === ConnectionStatusEnum.Online) {
            //   this.eventCtrl.publish('network:offline');
            // }
            // this.previousStatus = ConnectionStatusEnum.Offline;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Offline) {
                _this.setStatus(ConnectionStatusEnum.Online);
                _this.eventCtrl.publish('network:online');
            }
            // setTimeout(() => {
            //   if (this.previousStatus === ConnectionStatusEnum.Offline) {
            //     this.eventCtrl.publish('network:online');
            //   }
            //   this.previousStatus = ConnectionStatusEnum.Online;
            // }, 3000);
        });
    };
    NetworkProvider.prototype.getNetworkType = function () {
        return this.network.type;
    };
    NetworkProvider.prototype.getNetworkStatus = function () {
        return this._status.asObservable();
    };
    NetworkProvider.prototype.setStatus = function (status) {
        this.status = status;
        this._status.next(this.status);
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuProvider = /** @class */ (function () {
    function MenuProvider(http) {
        this.http = http;
    }
    MenuProvider.prototype.getSideMenus = function () {
        return [{
                title: 'Home', component: 'DashboardPage', icon: 'home'
            }, {
                title: 'Groups', component: 'GroupsPage', icon: 'people'
            }, {
                title: 'Customers', component: 'CustomersPage', icon: 'contacts'
            }, {
                title: 'Notifications', component: 'AllNotificationsPage', icon: 'notifications'
            },
            {
                title: 'Reports',
                subPages: [{
                        title: 'Daily Report',
                        component: 'DailyReportPage',
                    }, {
                        title: 'Speed Variation Report',
                        component: 'SpeedRepoPage',
                    }, {
                        title: 'Summary Report',
                        component: 'DeviceSummaryRepoPage',
                    }, {
                        title: 'Geofenceing Report',
                        component: 'GeofenceReportPage',
                    },
                    {
                        title: 'Overspeed Report',
                        component: 'OverSpeedRepoPage',
                    }, {
                        title: 'Ignition Report',
                        component: 'IgnitionReportPage',
                    },
                    {
                        title: 'Route Violation Report',
                        component: 'RouteVoilationsPage',
                    }, {
                        title: 'Stoppage Report',
                        component: 'StoppagesRepoPage',
                    },
                    {
                        title: 'Fuel Report',
                        component: 'FuelReportPage',
                    }, {
                        title: 'Distnace Report',
                        component: 'DistanceReportPage',
                    },
                    {
                        title: 'Trip Report',
                        component: 'TripReportPage',
                    }],
                icon: 'clipboard'
            },
            {
                title: 'Routes',
                subPages: [{
                        title: 'Routes',
                        component: 'RoutePage',
                    }, {
                        title: 'Route Maping',
                        component: 'RouteMapPage',
                    }],
                icon: 'map'
            }, {
                title: 'Feedback', component: 'FeedbackPage', icon: 'paper'
            }, {
                title: 'Contact Us', component: 'ContactUsPage', icon: 'mail'
            }, {
                title: 'Support', component: 'SupportPage', icon: 'call'
            }, {
                title: 'About Us', component: 'AboutUsPage', icon: 'alert'
            },];
    };
    MenuProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], MenuProvider);
    return MenuProvider;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(316);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\shared\side-menu-content\side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n        <!-- It is a simple option -->\n        <ng-template [ngIf]="!option.suboptionsCount">\n            <ion-item class="option" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                (tap)="select(option)" tappable>\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n                {{ option.displayText }}\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n            </ion-item>\n        </ng-template>\n\n        <!-- It has nested options -->\n        <ng-template [ngIf]="option.suboptionsCount">\n\n            <ion-list no-margin class="accordion-menu">\n\n                <!-- Header -->\n                <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                    (tap)="toggleItemOptions(option)" tappable>\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon"\n                        item-left></ion-icon>\n                    <!-- <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon> -->\n                    {{ option.displayText }}\n                </ion-item>\n\n                <!-- Sub items -->\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n                            tappable (tap)="select(item)">\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n                            {{ item.displayText }}\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n                        </ion-item>\n                    </ng-template>\n                </div>\n            </ion-list>\n\n        </ng-template>\n\n    </ng-template>\n</ion-list>'/*ion-inline-end:"D:\Pro\ionic-skystargps\shared\side-menu-content\side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { DatePicker } from '@ionic-native/date-picker';
// import * as moment from 'moment';
var FilterPage = /** @class */ (function () {
    function FilterPage(apiCall, viewCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.modal = {};
        this.filterList = [
            { filterID: 1, filterName: "Overspeed", filterValue: 'overspeed', checked: false },
            { filterID: 2, filterName: "Ignition", filterValue: 'IGN', checked: false },
            { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
            { filterID: 4, filterName: "Geofence", filterValue: 'Geo-Fence', checked: false },
            { filterID: 5, filterName: "Stoppage", filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 6, filterName: "Fuel", filterValue: 'Fuel', checked: false },
            { filterID: 7, filterName: "AC", filterValue: 'AC', checked: false },
            { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
            { filterID: 9, filterName: "Power", filterValue: 'power', checked: false }
        ];
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData);
        }
    }
    FilterPage.prototype.ngOnInit = function () {
    };
    // dateOpen() {
    //     this.datePicker.show({
    //         date: new Date(),
    //         mode: 'date',
    //         androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    //     }).then(
    //         date => console.log('Got date: ', date),
    //         err => console.log('Error occurred while getting date: ', err)
    //     );
    // }
    // changeDTformat(fromDT) {
    //     let that = this;
    //     that.modal.fromDate = moment(new Date(fromDT), "YYYY-MM-DD").format('dd/mm/yyyy');
    //     console.log("moment=> ", that.modal.fromDate);
    // }
    FilterPage.prototype.applyFilter = function () {
        console.log(this.modal);
        console.log(this.selectedArray);
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));
        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates");
            this.viewCtrl.dismiss(this.modal);
        }
        else {
            localStorage.setItem("types", "types");
            this.viewCtrl.dismiss(this.selectedArray);
        }
    };
    FilterPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.selectMember = function (data) {
        console.log("selected=> " + JSON.stringify(data));
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }
    };
    FilterPage.prototype.cancel = function () {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\all-notifications\filter\filter.html"*/'<ion-list>\n\n    <!-- <button ion-item (click)="close()">Close</button> -->\n\n    <!-- <ion-list-header style="text-align: center;">\n\n        Filter By\n\n    </ion-list-header>\n\n    <hr> -->\n\n    <ion-item-group>\n\n        <ion-item-divider color="light">Filter Type</ion-item-divider>\n\n        <div *ngIf="filterList.length > 0">\n\n            <ion-item *ngFor="let member of filterList">\n\n                <ion-label>{{member?.filterName || \'N/A\'}}</ion-label>\n\n                <ion-checkbox color="danger" (click)="selectMember(member)" [(ngModel)]="member.checked"></ion-checkbox>\n\n            </ion-item>\n\n        </div>\n\n    </ion-item-group>\n\n    <!-- <ion-item-group>\n\n        <ion-item-divider color="light">Time</ion-item-divider>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb2">From</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.fromDate"/>\n\n        </div>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb1">To</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.toDate"/>\n\n        </div>\n\n    </ion-item-group> -->\n\n\n\n\n\n\n\n</ion-list>\n\n<ion-row style="background-color: darkgray">\n\n    <ion-col width-50 style="text-align: center; background-color: #f53d3d;">\n\n        <button ion-button clear small color="light" (click)="cancel()">Cancel</button>\n\n    </ion-col>\n\n    <ion-col width-50 style="text-align: center; background-color: #11b700;">\n\n        <button ion-button clear small color="light" (click)="applyFilter()">Apply Filter</button>\n\n    </ion-col>\n\n</ion-row>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\all-notifications\filter\filter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ServiceProviderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UpdatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { TextToSpeech } from '@ionic-native/text-to-speech';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(appVersion, apiCall, alertCtrl, navCtrl, navParams, events, formBuilder) {
        var _this = this;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.formBuilder = formBuilder;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
            console.log("app version=> " + _this.aVer);
        });
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.footerState = __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.credentialsForm = this.formBuilder.group({
            fname: [this.islogin.fn],
            lname: [this.islogin.ln],
            email: [this.islogin.email],
            phonenum: [this.islogin.phn],
            org: [this.islogin._orgName],
        });
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ProfilePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ProfilePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    ProfilePage.prototype.settings = function () {
        this.navCtrl.push('SettingsPage');
    };
    ProfilePage.prototype.service = function () {
        this.navCtrl.push(ServiceProviderPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.password = function () {
        this.navCtrl.push(UpdatePasswordPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.onSignIn = function () {
        var _this = this;
        // this.logger.info('SignInPage: onSignIn()');
        console.log(this.credentialsForm.value);
        var data = {
            "fname": this.credentialsForm.value.fname,
            "lname": this.credentialsForm.value.lname,
            "org": this.credentialsForm.value.org,
            "noti": true,
            "uid": this.islogin._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.updateprofile(data)
            .subscribe(function (resdata) {
            _this.apiCall.stopLoading();
            console.log("response from server=> ", resdata);
            if (resdata.token) {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Profile updated succesfully!',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                var logindata = JSON.stringify(resdata);
                                var logindetails = JSON.parse(logindata);
                                var userDetails = window.atob(logindetails.token.split('.')[1]);
                                var details = JSON.parse(userDetails);
                                console.log(details.email);
                                localStorage.setItem("loginflag", "loginflag");
                                localStorage.setItem('details', JSON.stringify(details));
                                localStorage.setItem('condition_chk', details.isDealer);
                                _this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                                _this.footerState = 1;
                                _this.toggleFooter();
                            }
                        }]
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error from service=> ", err);
        });
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {
            "uid": this.islogin._id,
            "token": this.token,
            "os": "android"
        };
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            // this.menuCtrl.close();
                            _this.navCtrl.setRoot('LoginPage');
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        // this.menuCtrl.close();
                    }
                }]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\profile\profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Me</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (tap)="toggleFooter()">\n        <ion-icon color="light" name="create" *ngIf="footerState == 0"></ion-icon>\n        <ion-icon color="light" name="done-all" *ngIf="footerState == 1"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item>\n      <ion-avatar item-start>\n        <img src="assets/imgs/dummy-user-img.png">\n      </ion-avatar>\n      <h2>{{islogin.fn}}&nbsp;{{islogin.ln}}</h2>\n      <p>Account:{{islogin.account}}</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item (tap)="service()" *ngIf="!isDealer"> -->\n    <ion-item (tap)="service()">\n      <ion-icon name="person" item-start></ion-icon>\n      Service Provider\n    </ion-item>\n    <ion-item (tap)="password()">\n      <ion-icon name="lock" item-start></ion-icon>\n      Password\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item>\n      <ion-icon *ngIf="isChecked" name="mic" item-start></ion-icon>\n      <ion-icon *ngIf="!isChecked" name="mic-off" item-start></ion-icon>\n      <ion-label>Voice Notification</ion-label>\n      <ion-checkbox color="dark" [(ngModel)]="notif" (click)="setNotif(notif)" item-right></ion-checkbox>\n    </ion-item> -->\n    <ion-item (tap)="settings()">\n      <ion-icon name="cog" item-start></ion-icon>\n      Settings\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group (tap)="logout()">\n    <ion-item ion-text text-center>\n      Logout\n    </ion-item>\n  </ion-item-group>\n</ion-content>\n<ion-footer padding-bottom padding-top>\n  <div style="text-align: center;">Version {{aVer}}</div>\n  <br/>\n  <div id="photo" style="text-align: center">\n    <span style="vertical-align:middle">Powered by </span>&nbsp;\n    <a href="http://www.oneqlik.in/telematics/">\n      <img style="vertical-align:middle" src="assets/imgs/sign.jpg" width="55" height="19">\n    </a>\n  </div>\n</ion-footer>\n\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n  <!-- <ion-pullup-tab [footer]="pullup" (tap)="toggleFooter()">\n    <ion-icon name="arrow-up" *ngIf="footerState == 0"></ion-icon><ion-icon name="arrow-down" *ngIf="footerState == 1"></ion-icon>\n  </ion-pullup-tab>\n  <ion-toolbar color="primary" (tap)="toggleFooter()">\n    <ion-title>Footer</ion-title>\n  </ion-toolbar> -->\n  <ion-content>\n    <ion-row padding>\n      <ion-col style="text-align: center; font-size: 15px">Update Profile</ion-col>\n    </ion-row>\n    <form [formGroup]="credentialsForm">\n      <ion-item>\n        <ion-label floating>First Name</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'fname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Last Name</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'lname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Email Id</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'email\']" type="email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Phone Number</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'phonenum\']" type="number"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Organisation</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'org\']" type="text"></ion-input>\n      </ion-item>\n      <!-- <ion-item>\n        <ion-label>Timezone</ion-label>\n        <ion-select interface="popover">\n          <ion-option value="f">Female</ion-option>\n          <ion-option value="m">Male</ion-option>\n        </ion-select>\n      </ion-item> -->\n      <!-- <ion-item no-lines>\n        <ion-label>Dealer</ion-label>\n        <ion-checkbox item-right color="green" checked="true" *ngIf="islogin.isDealer"></ion-checkbox>\n        <ion-checkbox item-right color="gpsc" checked="true" *ngIf="!islogin.isDealer"></ion-checkbox>\n      </ion-item> -->\n\n      <ion-row>\n        <ion-col text-center>\n          <button ion-button block color="gpsc" (click)="onSignIn()">\n            Update Account\n          </button>\n        </ion-col>\n      </ion-row>\n\n    </form>\n  </ion-content>\n</ion-pullup>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]])
    ], ProfilePage);
    return ProfilePage;
}());

var ServiceProviderPage = /** @class */ (function () {
    function ServiceProviderPage(navParam) {
        this.navParam = navParam;
        this.uData = {};
        this.sorted = [];
        this.uData = this.navParam.get("param");
        if (this.uData.Dealer_ID != undefined) {
            this.sorted = this.uData.Dealer_ID;
        }
        else {
            this.sorted.first_name = this.uData.fn;
            this.sorted.last_name = this.uData.fn;
            this.sorted.phone = this.uData.phn;
            this.sorted.email = this.uData.email;
        }
        console.log("udata=> ", this.uData);
        console.log("udata=> ", JSON.stringify(this.uData));
    }
    ServiceProviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\profile\service-provider.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Provider Info</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <div class="div">\n\n        <img src="assets/imgs/dummy_user.jpg">\n\n    </div>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Name</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.first_name}}&nbsp;{{sorted.last_name}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Contacts</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Mobile</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>E-mail</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.email}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Address</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">N/A</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\profile\service-provider.html"*/,
            selector: 'page-profile'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ServiceProviderPage);
    return ServiceProviderPage;
}());

var UpdatePasswordPage = /** @class */ (function () {
    function UpdatePasswordPage(navParam, alertCtrl, toastCtrl, apiSrv, navCtrl) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiSrv = apiSrv;
        this.navCtrl = navCtrl;
        this.passData = this.navParam.get("param");
        console.log("passData=> " + JSON.stringify(this.passData));
    }
    UpdatePasswordPage.prototype.savePass = function () {
        var _this = this;
        if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
            var alert_2 = this.alertCtrl.create({
                message: 'Fields should not be empty!',
                buttons: ['OK']
            });
            alert_2.present();
        }
        else {
            if (this.newP != this.cnewP) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Password Missmatched!!',
                    buttons: ['Try Again']
                });
                alert_3.present();
            }
            else {
                var data = {
                    "ID": this.passData._id,
                    "OLD_PASS": this.oldP,
                    "NEW_PASS": this.newP
                };
                this.apiSrv.startLoading().present();
                this.apiSrv.updatePassword(data)
                    .subscribe(function (respData) {
                    _this.apiSrv.stopLoading();
                    console.log("respData=> ", respData);
                    var toast = _this.toastCtrl.create({
                        message: 'Password Updated successfully',
                        position: "bottom",
                        duration: 2000
                    });
                    toast.onDidDismiss(function () {
                        _this.oldP = "";
                        _this.newP = "";
                        _this.cnewP = "";
                    });
                    toast.present();
                }, function (err) {
                    _this.apiSrv.stopLoading();
                    console.log("error in update password=> ", err);
                    // debugger
                    if (err.message == "Timeout has occurred") {
                        // alert("the server is taking much time to respond. Please try in some time.")
                        var alerttemp = _this.alertCtrl.create({
                            message: "the server is taking much time to respond. Please try in some time.",
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        _this.navCtrl.setRoot("DashboardPage");
                                    }
                                }]
                        });
                        alerttemp.present();
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: err._body.message,
                            position: "bottom",
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            _this.oldP = "";
                            _this.newP = "";
                            _this.cnewP = "";
                            _this.navCtrl.setRoot("DashboardPage");
                        });
                        toast.present();
                    }
                });
            }
        }
    };
    UpdatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\profile\update-password.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Change Password</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="savePass()">\n\n                Save\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Old Password" [(ngModel)]="oldP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="New Password" [(ngModel)]="newP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Confirm New Password" [(ngModel)]="cnewP"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\profile\update-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], UpdatePasswordPage);
    return UpdatePasswordPage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicesPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__ = __webpack_require__(260);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddDevicesPage = /** @class */ (function () {
    function AddDevicesPage(navCtrl, navParams, apiCall, alertCtrl, toastCtrl, sms, modalCtrl, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.sms = sms;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.allDevices = [];
        this.allDevicesSearch = [];
        this.option_switch = false;
        this.searchCountryString = ''; // initialize your searchCountryString string empty
        this.page = 0;
        this.limit = 5;
        this.searchQuery = '';
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.isSuperAdmin = this.islogin.isSuperAdmin;
        this.islogindealer = localStorage.getItem('isDealervalue');
        console.log("islogindealer=> " + this.islogindealer);
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
        }
        else {
            // localStorage.removeItem("status");
            this.stausdevice = undefined;
        }
    }
    AddDevicesPage.prototype.ngOnInit = function () {
        // if (localStorage.getItem("SCREEN") != null) {
        //   this.navBar.backButtonClick = (e: UIEvent) => {
        //     if (localStorage.getItem("SCREEN") != null) {
        //       if (localStorage.getItem("SCREEN") === 'live') {
        //         this.navCtrl.setRoot('LivePage');
        //       } else {
        //         if (localStorage.getItem("SCREEN") === 'dashboard') {
        //           this.navCtrl.setRoot('DashboardPage')
        //         }
        //       }
        //     }
        //   }
        // }
        // this.getdevices();
        // console.log(this.isDealer);
        this.now = new Date().toISOString();
        // this.now = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:s');
    };
    AddDevicesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        this.getdevices();
        console.log(this.isDealer);
    };
    AddDevicesPage.prototype.activateVehicle = function (data) {
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": data
        });
    };
    AddDevicesPage.prototype.timeoutAlert = function () {
        var _this = this;
        var alerttemp = this.alertCtrl.create({
            message: "the server is taking much time to respond. Please try in some time.",
            buttons: [{
                    text: 'Okay',
                    handler: function () {
                        _this.navCtrl.setRoot("DashboardPage");
                    }
                }]
        });
        alerttemp.present();
    };
    AddDevicesPage.prototype.showVehicleDetails = function (vdata) {
        this.navCtrl.push('VehicleDetailsPage', {
            param: vdata
        });
    };
    AddDevicesPage.prototype.doRefresh = function (refresher) {
        var that = this;
        that.page = 0;
        that.limit = 5;
        console.log('Begin async operation', refresher);
        this.getdevices();
        refresher.complete();
    };
    AddDevicesPage.prototype.shareVehicle = function (d_data) {
        var that = this;
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            inputs: [
                {
                    name: 'device_name',
                    value: d_data.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        that.sharedevices(data, d_data);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.sharedevices = function (data, d_data) {
        var _this = this;
        var that = this;
        var devicedetails = {
            "did": d_data._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    AddDevicesPage.prototype.showDeleteBtn = function (b) {
        debugger;
        var that = this;
        if (localStorage.getItem('isDealervalue') == 'true') {
            return false;
        }
        else {
            if (b) {
                var u = b.split(",");
                for (var p = 0; p < u.length; p++) {
                    if (that.islogin._id == u[p]) {
                        return true;
                    }
                }
            }
            else {
                return false;
            }
        }
    };
    AddDevicesPage.prototype.sharedVehicleDelete = function (device) {
        var that = this;
        that.deivceId = device;
        var alert = that.alertCtrl.create({
            message: 'Do you want to delete this share vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        that.removeDevice(that.deivceId._id);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    AddDevicesPage.prototype.removeDevice = function (did) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
            .subscribe(function (data) {
            console.log(data);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: 'Shared Device was deleted successfully!',
                duration: 1500
            });
            toast.onDidDismiss(function () {
                _this.getdevices();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddDevicesPage.prototype.showSharedBtn = function (a, b) {
        // debugger
        if (b) {
            return !(b.split(",").indexOf(a) + 1);
        }
        else {
            return true;
        }
    };
    AddDevicesPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var popover = this.popoverCtrl.create(PopoverPage, {
            vehData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this.getdevices();
        });
        popover.present({
            ev: ev
        });
    };
    AddDevicesPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            if (that.stausdevice) {
                baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
            }
            else {
                baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
            }
            if (_this.islogin.isSuperAdmin == true) {
                baseURLp += '&supAdmin=' + _this.islogin._id;
            }
            else {
                if (_this.islogin.isDealer == true) {
                    baseURLp += '&dealer=' + _this.islogin._id;
                }
            }
            that.ndata = [];
            that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (res) {
                that.ndata = res.devices;
                for (var i = 0; i < that.ndata.length; i++) {
                    that.allDevices.push(that.ndata[i]);
                }
                that.allDevicesSearch = that.allDevices;
            }, function (error) {
                console.log(error);
            });
            infiniteScroll.complete();
        }, 500);
    };
    AddDevicesPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            var that = _this;
            // that.userPermission = localStorage.getItem('condition_chk');
            // if (that.userPermission == 'true') {
            //   that.option_switch = true;
            // } else {
            //   if (that.userPermission == 'false') {
            //     that.option_switch = false;
            //   }
            // }
            debugger;
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
            ///////////////////////////////
        }, function (err) {
            console.log("error=> ", err);
            _this.apiCall.stopLoading();
            // if (err.message == "Timeout has occurred") {
            //   this.timeoutAlert();
            // }
        });
    };
    AddDevicesPage.prototype.livetrack = function (device) {
        localStorage.setItem("LiveDevice", "LiveDevice");
        this.navCtrl.push('LiveSingleDevice', {
            device: device
        });
    };
    AddDevicesPage.prototype.showHistoryDetail = function (device) {
        this.navCtrl.push('HistoryDevicePage', {
            device: device
        });
    };
    AddDevicesPage.prototype.device_address = function (device, index) {
        var that = this;
        that.allDevices[index].address = "N/A";
        if (!device.last_location) {
            that.allDevices[index].address = "N/A";
        }
        else if (device.last_location) {
            var lattitude = device.last_location.lat;
            var longitude = device.last_location.long;
            var geocoder = new google.maps.Geocoder;
            var latlng = new google.maps.LatLng(lattitude, longitude);
            var request = {
                "latLng": latlng
            };
            geocoder.geocode(request, function (resp, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (resp[0] != null) {
                        that.allDevices[index].address = resp[0].formatted_address;
                    }
                    else {
                        console.log("No address available");
                    }
                }
                else {
                    that.allDevices[index].address = 'N/A';
                }
            });
        }
    };
    AddDevicesPage.prototype.callSearch = function (ev) {
        var _this = this;
        console.log(ev.target.value);
        var searchKey = ev.target.value;
        var _baseURL;
        if (this.islogin.isDealer == true) {
            _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        }
        else {
            if (this.islogin.isSuperAdmin == true) {
                _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
            }
            else {
                _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
            }
        }
        // this.apiCall.startLoading().present();
        // this.apiCall.callSearchService(this.islogin.email, this.islogin._id, searchKey)
        this.apiCall.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.allDevicesSearch = data.devices;
            _this.allDevices = data.devices;
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        this.messages = undefined;
        this.dataEngine = d;
        var baseURLp = 'https://www.oneqlik.in/deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            // debugger;
            _this.DeviceConfigStatus = data;
            _this.immobType = data[0].imobliser_type;
            // console.log("DeviceConfigStatus=>   " + JSON.stringify(this.DeviceConfigStatus))
            if (_this.dataEngine.ignitionLock == '1') {
                _this.messages = 'Do you want to unlock the engine?';
            }
            else {
                if (_this.dataEngine.ignitionLock == '0') {
                    _this.messages = 'Do you want to lock the engine?';
                }
            }
            // let profileModal = this.modalCtrl.create('ImmobilizePage', { param: d, udata: data, msg: this.messages });
            // profileModal.present();
            var alert = _this.alertCtrl.create({
                message: _this.messages,
                buttons: [{
                        text: 'YES',
                        handler: function () {
                            // debugger;
                            if (_this.immobType == 0 || _this.immobType == undefined) {
                                var devicedetail = {
                                    "_id": _this.dataEngine._id,
                                    "engine_status": !_this.dataEngine.engine_status
                                };
                                _this.apiCall.startLoading().present();
                                _this.apiCall.deviceupdateCall(devicedetail)
                                    .subscribe(function (response) {
                                    _this.apiCall.stopLoading();
                                    _this.editdata = response;
                                    var toast = _this.toastCtrl.create({
                                        message: response.message,
                                        duration: 2000,
                                        position: 'top'
                                    });
                                    toast.present();
                                    _this.responseMessage = "Edit successfully";
                                    _this.getdevices();
                                    var msg;
                                    if (!_this.dataEngine.engine_status) {
                                        msg = _this.DeviceConfigStatus[0].resume_command;
                                    }
                                    else {
                                        msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                    }
                                    _this.sms.send(d.sim_number, msg);
                                    var toast1 = _this.toastCtrl.create({
                                        message: 'SMS sent successfully',
                                        duration: 2000,
                                        position: 'bottom'
                                    });
                                    toast1.present();
                                }, function (error) {
                                    _this.apiCall.stopLoading();
                                    console.log(error);
                                });
                            }
                            else {
                                // call othere service
                                console.log("Call server code here!!");
                                // debugger;
                                var data = {
                                    "imei": d.Device_ID,
                                    "_id": _this.dataEngine._id,
                                    "engine_status": d.ignitionLock,
                                    "protocol_type": d.device_model.device_type
                                };
                                _this.apiCall.startLoading().present();
                                _this.apiCall.serverLevelonoff(data)
                                    .subscribe(function (resp) {
                                    _this.apiCall.stopLoading();
                                    console.log("ignition on off=> ", resp);
                                    _this.respMsg = resp;
                                    _this.apiCall.startLoadingnew(_this.dataEngine.ignitionLock).present();
                                    _this.intervalID = setInterval(function () {
                                        _this.apiCall.callResponse(_this.respMsg._id)
                                            .subscribe(function (data) {
                                            console.log("interval=> " + data);
                                            _this.commandStatus = data.status;
                                            if (_this.commandStatus == 'SUCCESS') {
                                                clearInterval(_this.intervalID);
                                                _this.apiCall.stopLoading();
                                                var toast1 = _this.toastCtrl.create({
                                                    message: 'process has completed successfully!',
                                                    duration: 1500,
                                                    position: 'bottom'
                                                });
                                                toast1.present();
                                                _this.getdevices();
                                            }
                                        });
                                    }, 5000);
                                    // this.getdevices();
                                }, function (err) {
                                    _this.apiCall.stopLoading();
                                    console.log("error in onoff=>", err);
                                    // if (err.message == "Timeout has occurred") {
                                    //   this.timeoutAlert();
                                    // }
                                });
                            }
                        }
                    },
                    {
                        text: 'No'
                    }]
            });
            alert.present();
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
        // debugger;
    };
    ;
    AddDevicesPage.prototype.dialNumber = function (number) {
        window.open('tel:' + number, '_system');
    };
    AddDevicesPage.prototype.getItems = function (ev) {
        console.log(ev.target.value, this.allDevices);
        var val = ev.target.value.trim();
        this.allDevicesSearch = this.allDevices.filter(function (item) {
            return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.allDevicesSearch);
    };
    AddDevicesPage.prototype.onClear = function (ev) {
        // debugger;
        this.getdevices();
        ev.target.value = '';
    };
    AddDevicesPage.prototype.openAdddeviceModal = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create('AddDeviceModalPage');
        profileModal.onDidDismiss(function (data) {
            console.log(data);
            _this.getdevices();
        });
        profileModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], AddDevicesPage.prototype, "navBar", void 0);
    AddDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\add-devices\add-devices.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Vehicle List</ion-title>\n    <!-- <ion-buttons end *ngIf="isDealer || islogindealer"> -->\n    <ion-buttons end *ngIf="isDealer || isSuperAdmin">\n      <button ion-button icon-only (click)="openAdddeviceModal()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <!-- <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar> -->\n  <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-card *ngFor="let d of allDevicesSearch; let i=index;">\n    <div *ngIf="d.expiration_date < now " style="position: relative">\n      <div>\n        <ion-item style="background-color: rgba(0, 0, 0, 0.9)">\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'car\')">\n            <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'truck\')">\n            <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bike\')">\n            <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'jcb\')">\n            <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bus\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'user\')">\n          </ion-avatar>\n\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'car\')">\n            <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'truck\')">\n            <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bike\')">\n            <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'jcb\')">\n            <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bus\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'user\')">\n          </ion-avatar>\n\n          <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'car\')">\n            <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'truck\')">\n            <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'bike\')">\n            <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'jcb\')">\n            <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'bus\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'user\')">\n          </ion-avatar>\n\n          <div>\n            <h2 style="color: gray">{{d.Device_Name}}</h2>\n            <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n              <span style="text-transform: uppercase; color: red;">{{d.status}} </span>\n              <span *ngIf="d.status_updated_at ">since&nbsp;{{d.status_updated_at | date:\'medium\'}} </span>\n            </p>\n          </div>\n\n          <button ion-button item-end clear *ngIf="!showDeleteBtn(d.SharedWith)">\n            <ion-icon ios="ios-more" md="md-more" color="grey" *ngIf="option_switch"></ion-icon>\n          </button>\n\n          <button ion-button item-end clear *ngIf="!option_switch">\n            <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id,d.SharedWith)"></ion-icon>\n          </button>\n\n          <button ion-button item-end clear *ngIf="showDeleteBtn(d.SharedWith)">\n            <ion-icon ios="ios-remove-circle" md="md-remove-circle" color="danger"></ion-icon>\n          </button>\n\n          <div item-end *ngIf="d.currentFuel">\n            <ion-avatar class="ava">\n              <img src="assets/imgs/fuel.png">\n            </ion-avatar>\n            <p style="color: gray; font-size: 10px;font-weight: 400;margin:0px;">{{d.currentFuel}}L</p>\n          </div>\n        </ion-item>\n\n        <ion-row style="background-color: rgba(0, 0, 0, 0.9)" padding-right>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="pin" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Live</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="clock" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">History</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="power" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Ignition</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="lock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock==\'1\'"></ion-icon>\n            <ion-icon name="unlock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock==\'0\'"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Immobilize</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="battery-charging" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Power</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="call" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Driver</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-item item-start class="itemStyle" style="background-color: rgba(0, 0, 0, 0.9)">\n          <ion-row>\n            <ion-col (onCreate)="device_address(d,i)" class="colSt2">\n              <div class="overme" style="color: gray">\n                {{d.address}}\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </div>\n      <div>\n        <button class="activeBtn" ion-button outline color="secondary" (tap)="activateVehicle(d)">Activate</button>\n      </div>\n    </div>\n   \n    <div *ngIf="(d.expiration_date == null) || (d.expiration_date > now)">\n      <ion-item>\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null" (click)="livetrack(d)">\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\' || d.vehicleType.iconType == \'Heavy Truck\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\' || d.vehicleType.iconType == \'Heavy Truck\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\' || d.vehicleType.iconType == \'Heavy Truck\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'user\')&&( !d.last_ACC ))">\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null" (click)="livetrack(d)">\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'bus\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.vehicleType.iconType == \'user\')&&( !d.last_ACC ))">\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null" (click)="livetrack(d)">\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'car\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'bus\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'user\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'user\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'user\')&&( !d.last_ACC ))">\n        </ion-avatar>\n\n        <div (tap)="showVehicleDetails(d)">\n          <h2>{{d.Device_Name}}</h2>\n          <p style="color:#cf1c1c;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n            <span style="text-transform: uppercase;">{{d.status}} </span>\n            <span *ngIf="d.status_updated_at ">since&nbsp;{{d.status_updated_at | date:\'medium\'}} </span>\n          </p>\n        </div>\n\n        <button ion-button item-end clear (click)="presentPopover($event, d)" *ngIf="!showDeleteBtn(d.SharedWith)">\n          <ion-icon ios="ios-more" md="md-more" *ngIf="option_switch"></ion-icon>\n        </button>\n\n        <button ion-button item-end clear (click)="shareVehicle(d)" *ngIf="!option_switch">\n          <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id,d.SharedWith)"></ion-icon>\n        </button>\n\n        <button ion-button item-end clear (click)="sharedVehicleDelete(d)" *ngIf="showDeleteBtn(d.SharedWith)">\n          <ion-icon ios="ios-remove-circle" md="md-remove-circle" color="danger"></ion-icon>\n        </button>\n\n        <div item-end *ngIf="d.currentFuel">\n          <ion-avatar class="ava">\n            <img src="assets/imgs/fuel.png">\n          </ion-avatar>\n          <p style="color: green; font-size: 10px;font-weight: 400;margin:0px;">{{d.currentFuel}}L</p>\n        </div>\n      </ion-item>\n\n      <ion-row style="background-color: #fafafa;" padding-right>\n        <ion-col width-20 style="text-align:center" (click)="livetrack(d)">\n          <ion-icon name="pin" style="color:#ffc900;font-size: 12px;"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Live</p>\n        </ion-col>\n        <ion-col width-20 style="text-align:center" (click)="showHistoryDetail(d)">\n          <ion-icon name="clock" style="color:#d675ea;font-size: 12px;"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">History</p>\n        </ion-col>\n        <ion-col width-20 style="text-align:center">\n          <ion-icon name="power" style="color:#ef473a;font-size: 12px;" *ngIf="d.last_ACC==\'0\'"></ion-icon>\n          <ion-icon name="power" style="color:#1de21d;font-size: 12px;" *ngIf="d.last_ACC==\'1\'"></ion-icon>\n          <ion-icon name="power" style="color:gray;font-size: 12px;" *ngIf="d.last_ACC==null"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Ignition</p>\n        </ion-col>\n        <ion-col width-20 style="text-align:center" (click)="IgnitionOnOff(d)">\n          <ion-icon name="lock" style="color:#ef473a;font-size: 12px;" *ngIf="d.ignitionLock==\'1\'"></ion-icon>\n          <ion-icon name="unlock" style="color:#1de21d;font-size: 12px;" *ngIf="d.ignitionLock==\'0\'"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Immobilize</p>\n        </ion-col>\n        <ion-col width-20 style="text-align:center">\n          <ion-icon name="battery-charging" style="color:#ef473a;font-size: 12px;" *ngIf="d.power!=\'1\'"></ion-icon>\n          <ion-icon name="battery-charging" style="color:#1de21d;font-size: 12px;" *ngIf="d.power==\'1\'"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Power</p>\n        </ion-col>\n        <ion-col width-20 style="text-align:center" (click)="dialNumber(d.contact_number)">\n          <ion-icon name="call" style="color:#0000FF;font-size: 12px;"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Driver</p>\n        </ion-col>\n      </ion-row>\n      <ion-item item-start class="itemStyle" style="background:#696D74;">\n        <ion-row>\n          <ion-col (onCreate)="device_address(d,i)" class="colSt2">\n            <div class="overme">\n              {{d.address}}\n            </div>\n          </ion-col>\n          <ion-col class="colSt1">\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">Today\'s distance</p>\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">{{d.today_odo | number : \'1.0-2\'}}Kms</p>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </div>\n  </ion-card>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n\n'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\add-devices\add-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], AddDevicesPage);
    return AddDevicesPage;
}());

var PopoverPage = /** @class */ (function () {
    function PopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.vehData = navParams.get("vehData");
        console.log("popover data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    PopoverPage.prototype.ngOnInit = function () {
        // if (this.navParams.data) {
        //   this.contentEle = this.navParams.data.contentEle;
        //   this.textEle = this.navParams.data.textEle;
        // }
    };
    PopoverPage.prototype.editItem = function () {
        var _this = this;
        console.log("edit");
        var modal = this.modalCtrl.create('UpdateDevicePage', {
            vehData: this.vehData
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.viewCtrl.dismiss();
        });
        modal.present();
    };
    PopoverPage.prototype.deleteItem = function () {
        var that = this;
        console.log("delete");
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    PopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.deleteDeviceCall(d_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Vehicle deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                // this.navCtrl.push(AddDevicesPage);
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage.prototype.shareItem = function () {
        var that = this;
        console.log("share");
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'device_name',
                    value: that.vehData.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("clicked=> ", data);
                        that.sharedevices(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    PopoverPage.prototype.sharedevices = function (data) {
        var _this = this;
        var that = this;
        console.log(data.shareId);
        var devicedetails = {
            "did": that.vehData._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var editdata = data;
            console.log(editdata);
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            // });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage.prototype.upload = function () {
        var _this = this;
        var mod = this.modalCtrl.create('UploadDocPage');
        mod.present();
        mod.onDidDismiss(function () {
            _this.viewCtrl.dismiss();
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n  \n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteItem()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"upload()\">\n      <ion-icon name=\"cloud-upload\"></ion-icon>&nbsp;&nbsp;Upload Doc\n      </ion-item>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=add-devices.js.map

/***/ }),

/***/ 261:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(266);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyErrorHandler */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_api_service_api_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_network_network__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_menu_menu__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_pro__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_pro___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25__ionic_pro__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_storage__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__ = __webpack_require__(256);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















// Custom components














__WEBPACK_IMPORTED_MODULE_25__ionic_pro__["Pro"].init('c1019747', {
    appVersion: '0.0.1'
});
var MyErrorHandler = /** @class */ (function () {
    function MyErrorHandler(injector) {
        try {
            this.ionicErrorHandler = injector.get(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"]);
        }
        catch (e) {
            // Unable to get the IonicErrorHandler provider, ensure
            // IonicErrorHandler has been added to the providers list below
        }
    }
    MyErrorHandler.prototype.handleError = function (err) {
        __WEBPACK_IMPORTED_MODULE_25__ionic_pro__["Pro"].monitoring.handleNewError(err);
        // Remove this if you want to disable Ionic's auto exception handling
        // in development mode.
        this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
    };
    MyErrorHandler = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injector"]])
    ], MyErrorHandler);
    return MyErrorHandler;
}());

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_19__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/add-devices/immobilize/immobilize.module#ImmobilizePageModule', name: 'ImmobilizePage', segment: 'immobilize', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/payment-greeting/payment-greeting.module#PaymentGreetingPageModule', name: 'PaymentGreetingPage', segment: 'payment-greeting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/payment-secure/payment-secure.module#PaymentSecurePageModule', name: 'PaymentSecurePage', segment: 'payment-secure', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module#PaytmwalletloginPageModule', name: 'PaytmwalletloginPage', segment: 'paytmwalletlogin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/upload-doc.module#UploadDocPageModule', name: 'UploadDocPage', segment: 'upload-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/wallet/wallet.module#WalletPageModule', name: 'WalletPage', segment: 'wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/group-modal/group-modal.module#GroupModalPageModule', name: 'GroupModalPage', segment: 'group-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/update-cust/update-cust.module#UpdateCustModalPageModule', name: 'UpdateCustModalPage', segment: 'update-cust', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report/daily-report.module#DailyReportPageModule', name: 'DailyReportPage', segment: 'daily-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/add-dealer/add-dealer.module#AddDealerPageModule', name: 'AddDealerPage', segment: 'add-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/edit-dealer/edit-dealer.module#EditDealerPageModule', name: 'EditDealerPage', segment: 'edit-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/distance-report/distance-report.module#DistanceReportPageModule', name: 'DistanceReportPage', segment: 'distance-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device-summary-repo/device-summary-repo.module#DeviceSummaryRepoPageModule', name: 'DeviceSummaryRepoPage', segment: 'device-summary-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel-report/fuel-report.module#FuelReportPageModule', name: 'FuelReportPage', segment: 'fuel-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence-report/geofence-report.module#GeofenceReportPageModule', name: 'GeofenceReportPage', segment: 'geofence-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence-show/geofence-show.module#GeofenceShowPageModule', name: 'GeofenceShowPage', segment: 'geofence-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence.module#GeofencePageModule', name: 'GeofencePage', segment: 'geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ignition-report/ignition-report.module#IgnitionReportPageModule', name: 'IgnitionReportPage', segment: 'ignition-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/device-settings/device-settings.module#DeviceSettingsPageModule', name: 'DeviceSettingsPage', segment: 'device-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/expired/expired.module#ExpiredPageModule', name: 'ExpiredPage', segment: 'expired', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/over-speed-repo/over-speed-repo.module#OverSpeedRepoPageModule', name: 'OverSpeedRepoPage', segment: 'over-speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-map-show/route-map-show.module#RouteMapShowPageModule', name: 'RouteMapShowPage', segment: 'route-map-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-voilations/route-voilations.module#RouteVoilationsPageModule', name: 'RouteVoilationsPage', segment: 'route-voilations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-geofence/show-geofence.module#ShowGeofencePageModule', name: 'ShowGeofencePage', segment: 'show-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route/route.module#RoutePageModule', name: 'RoutePage', segment: 'route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-route/show-route.module#ShowRoutePageModule', name: 'ShowRoutePage', segment: 'show-route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-trip/show-trip.module#ShowTripPageModule', name: 'ShowTripPage', segment: 'show-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sos-report/sos-report.module#SosReportPageModule', name: 'SosReportPage', segment: 'sos-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speed-repo/speed-repo.module#SpeedRepoPageModule', name: 'SpeedRepoPage', segment: 'speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stoppages-repo/stoppages-repo.module#StoppagesRepoPageModule', name: 'StoppagesRepoPage', segment: 'stoppages-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-report.module#TripReportPageModule', name: 'TripReportPage', segment: 'trip-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/update-device/update-device.module#UpdateDevicePageModule', name: 'UpdateDevicePage', segment: 'update-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/all-notifications.module#AllNotificationsPageModule', name: 'AllNotificationsPage', segment: 'all-notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-customer-modal/add-customer-modal.module#AddCustomerModalModule', name: 'AddCustomerModal', segment: 'add-customer-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-device-modal.module#AddDeviceModalPageModule', name: 'AddDeviceModalPage', segment: 'add-device-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/dealers.module#DealerPageModule', name: 'DealerPage', segment: 'dealers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-review/trip-review.module#TripReviewPageModule', name: 'TripReviewPage', segment: 'trip-review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/vehicle-details/vehicle-details.module#VehicleDetailsPageModule', name: 'VehicleDetailsPage', segment: 'vehicle-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/add-geofence/add-geofence.module#AddGeofencePageModule', name: 'AddGeofencePage', segment: 'add-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-device/history-device.module#HistoryDevicePageModule', name: 'HistoryDevicePage', segment: 'history-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/add-devices.module#AddDevicesPageModule', name: 'AddDevicesPage', segment: 'add-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/live-single-device.module#LiveSingleDeviceModule', name: 'LiveSingleDevice', segment: 'live-single-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_26__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"],
                [{ provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: MyErrorHandler }],
                // { provide: ErrorHandler, useClass: IonicErrorHandler },
                __WEBPACK_IMPORTED_MODULE_10__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_11__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_12__providers_menu_menu__["a" /* MenuProvider */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["h" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__["a" /* FilePath */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_network__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, 
        // private network: Network,
        events, networkProvider, menuProvider, menuCtrl, modalCtrl, push, alertCtrl, app, apiCall, toastCtrl, tts, storage) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuProvider = menuProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.tts = tts;
        this.storage = storage;
        this.islogin = {};
        // Settings for the SideMenuContentComponent
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__["ReplaySubject"](0);
        // this.backgroundMode.enable();
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            console.log("islogin=> " + JSON.stringify(_this.islogin));
        });
        this.events.subscribe('notif:updated', function (notifData) {
            console.log("text to speech updated=> ", notifData);
            // this.notfiD = notifData;
            localStorage.setItem("notifValue", notifData);
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin=> " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
        console.log("DealerDetails=> " + this.DealerDetails._id);
        this.dealerStatus = this.islogin.isDealer;
        console.log("dealerStatus=> " + this.dealerStatus);
        this.getSideMenuData();
        this.initializeApp();
        if (localStorage.getItem("loginflag")) {
            // debugger;
            if (localStorage.getItem("SCREEN") != null) {
                if (localStorage.getItem("SCREEN") === 'live') {
                    this.rootPage = 'LivePage';
                }
                else {
                    if (localStorage.getItem("SCREEN") === 'dashboard') {
                        this.rootPage = 'DashboardPage';
                    }
                }
            }
            else {
                this.rootPage = 'DashboardPage';
            }
        }
        else {
            this.rootPage = 'LoginPage';
        }
    }
    MyApp.prototype.getSideMenuData = function () {
        this.pages = this.menuProvider.getSideMenus();
    };
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                console.log('We do not have permission to send push notifications');
            }
        });
    };
    MyApp.prototype.backBtnHandler = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            var nav = _this.app.getActiveNavs()[0];
            var activeView = nav.getActive();
            if (activeView.name == "DashboardPage") {
                if (nav.canGoBack()) {
                    nav.pop();
                }
                else {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Application exit prevented!');
                                }
                            }, {
                                text: 'Close App',
                                handler: function () {
                                    _this.platform.exitApp(); // Close this application
                                }
                            }]
                    });
                    alert_1.present();
                }
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            // if (this.backgroundMode.isEnabled()) {
            if (localStorage.getItem("notifValue") != null) {
                if (localStorage.getItem("notifValue") == 'true') {
                    _this.tts.speak(notification.message)
                        .then(function () { return console.log('Success'); })
                        .catch(function (reason) { return console.log(reason); });
                }
            }
            // }
            if (notification.additionalData.foreground) {
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
            _this.storage.set("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.pushNotify();
            that.networkProvider.initializeNetworkEvents();
            // Offline event
            that.events.subscribe('network:offline', function () {
                // alert('network:offline ==> ' + this.networkProvider.getNetworkType());
                alert("Internet is not connected... please make sure the internet connection is working properly.");
            });
            // Online event
            that.events.subscribe('network:online', function () {
                alert('network:online ==> ' + _this.networkProvider.getNetworkType());
            });
            that.backBtnHandler();
        });
        // Initialize some options
        that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.initializeOptions = function () {
        var _this = this;
        this.options = new Array();
        // debugger;
        // Load simple menu options analytics
        // ------------------------------------------
        debugger;
        if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && this.islogin.isDealer == false) {
            this.options.push({
                iconName: 'home',
                displayText: 'Home',
                component: 'DashboardPage',
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Groups',
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Dealers',
                component: 'DealerPage'
            });
            this.options.push({
                iconName: 'contacts',
                displayText: 'Customers',
                component: 'CustomersPage'
            });
            this.options.push({
                iconName: 'notifications',
                displayText: 'Notifications',
                component: 'AllNotificationsPage'
            });
            // Load options with nested items (with icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Reports',
                iconName: 'clipboard',
                suboptions: [
                    {
                        // iconName: 'clipboard',
                        displayText: 'Daily Report',
                        component: 'DailyReportPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Summary Report',
                        component: 'DeviceSummaryRepoPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Geofenceing Report',
                        component: 'GeofenceReportPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Overspeed Report',
                        component: 'OverSpeedRepoPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Ignition Report',
                        component: 'IgnitionReportPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Stoppage Report',
                        component: 'StoppagesRepoPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Fuel Report',
                        component: 'FuelReportPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Distance Report',
                        component: 'DistanceReportPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Trip Report',
                        component: 'TripReportPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Route Violation Report',
                        component: 'RouteVoilationsPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'Speed Variation Report',
                        component: 'SpeedRepoPage'
                    },
                    {
                        // iconName: 'clipboard',
                        displayText: 'SOS Report',
                        component: 'SosReportPage'
                    }
                ]
            });
            // Load options with nested items (without icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Routes',
                iconName: 'map',
                component: 'RoutePage'
            });
            // Load special options
            // -----------------------------------------------
            this.options.push({
                displayText: 'Customer Support',
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: 'Rate Us',
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: 'Contact Us',
                        component: 'ContactUsPage'
                    }
                ]
            });
            this.options.push({
                displayText: 'Profile',
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        else {
            if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
                this.options.push({
                    iconName: 'home',
                    displayText: 'Home',
                    component: 'DashboardPage',
                });
                this.options.push({
                    iconName: 'people',
                    displayText: 'Groups',
                    component: 'GroupsPage'
                });
                this.options.push({
                    iconName: 'notifications',
                    displayText: 'Notifications',
                    component: 'AllNotificationsPage'
                });
                // Load options with nested items (with icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Reports',
                    iconName: 'clipboard',
                    suboptions: [
                        {
                            // iconName: 'clipboard',
                            displayText: 'Daily Report',
                            component: 'DailyReportPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Summary Report',
                            component: 'DeviceSummaryRepoPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Geofenceing Report',
                            component: 'GeofenceReportPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Overspeed Report',
                            component: 'OverSpeedRepoPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Ignition Report',
                            component: 'IgnitionReportPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Stoppage Report',
                            component: 'StoppagesRepoPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Fuel Report',
                            component: 'FuelReportPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Distance Report',
                            component: 'DistanceReportPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Trip Report',
                            component: 'TripReportPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Route Violation Report',
                            component: 'RouteVoilationsPage'
                        },
                        {
                            // iconName: 'clipboard',
                            displayText: 'Speed Variation Report',
                            component: 'SpeedRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'SOS Report',
                            component: 'SosReportPage'
                        }
                    ]
                });
                // Load options with nested items (without icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Routes',
                    iconName: 'map',
                    component: 'RoutePage'
                });
                // Load special options
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Customer Support',
                    suboptions: [
                        {
                            iconName: 'star',
                            displayText: 'Rate Us',
                            component: 'FeedbackPage'
                        },
                        {
                            iconName: 'mail',
                            displayText: 'Contact Us',
                            component: 'ContactUsPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: 'Profile',
                    iconName: 'person',
                    component: 'ProfilePage'
                });
            }
            else {
                if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == true) {
                    this.options.push({
                        iconName: 'home',
                        displayText: 'Home',
                        component: 'DashboardPage',
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: 'Groups',
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'contacts',
                        displayText: 'Customers',
                        component: 'CustomersPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: 'Notifications',
                        component: 'AllNotificationsPage'
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Reports',
                        iconName: 'clipboard',
                        suboptions: [
                            {
                                // iconName: 'clipboard',
                                displayText: 'Daily Report',
                                component: 'DailyReportPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Summary Report',
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Geofenceing Report',
                                component: 'GeofenceReportPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Overspeed Report',
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Ignition Report',
                                component: 'IgnitionReportPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Stoppage Report',
                                component: 'StoppagesRepoPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Fuel Report',
                                component: 'FuelReportPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Distance Report',
                                component: 'DistanceReportPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Trip Report',
                                component: 'TripReportPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Route Violation Report',
                                component: 'RouteVoilationsPage'
                            },
                            {
                                // iconName: 'clipboard',
                                displayText: 'Speed Variation Report',
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'SOS Report',
                                component: 'SosReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Routes',
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Customer Support',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: 'Rate Us',
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: 'Contact Us',
                                component: 'ContactUsPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: 'Profile',
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
                else {
                    this.options.push({
                        iconName: 'home',
                        displayText: 'Home',
                        component: 'DashboardPage',
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: 'Groups',
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: 'Notifications',
                        component: 'AllNotificationsPage'
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Reports',
                        // iconName: 'clipboard',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: 'Daily Report',
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Summary Report',
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Geofenceing Report',
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Overspeed Report',
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Ignition Report',
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Stoppage Report',
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Fuel Report',
                                component: 'FuelReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Distnace Report',
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Trip Report',
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Route Violation Report',
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Speed Variation Report',
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'SOS Report',
                                component: 'SosReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Routes',
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Customer Support',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: 'Rate Us',
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: 'Contact Us',
                                component: 'ContactUsPage'
                            },
                        ]
                    });
                    this.options.push({
                        displayText: 'Profile',
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
            }
        }
        console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"));
        // var checkCon = localStorage.getItem("isDealervalue");
        // var supAdminValue = localStorage.getItem("isSuperAdminValue");
        // if (checkCon != null) {
        //   if (checkCon == 'true') {
        //     console.log("console=> ", localStorage.getItem("isDealervalue"))
        //     this.options[2].displayText = 'Dealers';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //   } else if (checkCon == 'false') {
        //     this.options[2].displayText = 'Customers';
        //     this.options[2].iconName = 'contacts';
        //     this.options[2].component = 'CustomersPage';
        //   }
        // }
        var _DealerStat = localStorage.getItem("dealer_status");
        var _CustStat = localStorage.getItem("custumer_status");
        var onlyDeal = localStorage.getItem("OnlyDealer");
        if (_DealerStat != null || _CustStat != null) {
            if (_DealerStat == 'ON' && _CustStat == 'OFF') {
                this.options[2].displayText = 'Admin';
                this.options[2].iconName = 'person';
                this.options[2].component = 'DashboardPage';
                this.options[3].displayText = 'Customers';
                this.options[3].iconName = 'contacts';
                this.options[3].component = 'CustomersPage';
            }
            else {
                if (_DealerStat == 'OFF' && _CustStat == 'ON') {
                    this.options[2].displayText = 'Dealers';
                    this.options[2].iconName = 'person';
                    this.options[2].component = 'DashboardPage';
                }
                else {
                    if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
                        this.options[2].displayText = 'Dealers';
                        this.options[2].iconName = 'person';
                        this.options[2].component = 'DealerPage';
                        this.options[3].displayText = 'Customers';
                        this.options[3].iconName = 'contacts';
                        this.options[3].component = 'CustomersPage';
                    }
                    else {
                        if (onlyDeal == 'true') {
                            this.options[2].displayText = 'Customers';
                            this.options[2].iconName = 'contacts';
                            this.options[2].component = 'CustomersPage';
                        }
                    }
                }
            }
        }
        // if (checkCon != null || supAdminValue != null) {
        //   if (checkCon == 'true' && supAdminValue == 'false') {
        //     // console.log("console=> ", localStorage.getItem("isDealervalue"))
        //     this.options[2].displayText = 'Admin';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //     this.options[3].displayText = 'Customers';
        //     this.options[3].iconName = 'contacts';
        //     this.options[3].component = 'CustomersPage';
        //   } else if (checkCon == 'false') {
        //     this.options[2].displayText = 'Customers';
        //     this.options[2].iconName = 'contacts';
        //     this.options[2].component = 'CustomersPage';
        //   }
        // }
        this.events.subscribe("sidemenu:event", function (data) {
            console.log("sidemenu:event=> ", data);
            if (data) {
                _this.options[2].displayText = 'Dealers';
                _this.options[2].iconName = 'person';
                _this.options[2].component = 'DashboardPage';
            }
        });
    };
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                // Get the params if any
                var params = option.custom && option.custom.param;
                console.log("side menu click event=> " + option.component);
                // Redirect to the selected page
                // if (localStorage.getItem("isDealervalue") && option.component == 'DashboardPage') {
                //   localStorage.setItem('details', localStorage.getItem('dealer'));
                //   localStorage.setItem('isDealervalue', 'false');
                // }
                // debugger;
                if (option.displayText == 'Admin' && option.component == 'DashboardPage') {
                    localStorage.setItem("dealer_status", 'OFF');
                    localStorage.setItem('details', localStorage.getItem("superAdminData"));
                    localStorage.removeItem('superAdminData');
                }
                if (option.displayText == 'Dealers' && option.component == 'DashboardPage') {
                    if (localStorage.getItem('custumer_status') == 'ON') {
                        var _dealdata = JSON.parse(localStorage.getItem("dealer"));
                        if (localStorage.getItem("superAdminData") != null || _this.islogin.isSuperAdmin == true) {
                            localStorage.setItem("dealer_status", 'ON');
                        }
                        else {
                            if (_dealdata.isSuperAdmin == true) {
                                localStorage.setItem("dealer_status", 'OFF');
                            }
                            else {
                                localStorage.setItem("OnlyDealer", "true");
                            }
                        }
                        localStorage.setItem("custumer_status", 'OFF');
                        localStorage.setItem('details', localStorage.getItem("dealer"));
                    }
                    else {
                        console.log("something wrong!!");
                    }
                }
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.chkCondition = function () {
        var _this = this;
        // debugger;
        this.events.subscribe("event_sidemenu", function (data) {
            // var sidemenuOption = JSON.parse(data);
            // console.log(data);
            // console.log(sidemenuOption);
            _this.islogin = JSON.parse(data);
            // console.log("islogin event publish=> " + this.islogin);
            _this.options[2].displayText = 'Dealers';
            _this.options[2].iconName = 'person';
            _this.options[2].component = 'DashboardPage';
            // this.events.publish("sidemenu:event", this.options)
            _this.initializeOptions();
            // console.log("options=> " + JSON.stringify(this.options[2]))
        });
        // this.events.subscribe('user:updated', (udata) => {
        //   this.islogin = udata;
        //   console.log("islogin=> " + JSON.stringify(this.islogin));
        //   console.log("isDealer=> ", udata.isDealer)
        //   if (udata.isDealer === false) {
        //     this.options[2].displayText = 'Dealers';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //   }
        //   this.initializeOptions();
        //   console.log(JSON.stringify(this.options))
        // });
        this.initializeOptions();
        // this.dealerChk = localStorage.getItem('condition_chk');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\app\app.html"*/'<ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)=\'chkCondition()\'>\n  <ion-header>\n    <!-- <ion-navbar style="background-image: linear-gradient(to right top, #d80622, #c0002d, #a60033, #8b0935, #6f1133);"> -->\n    <!-- <div class="headProf">\n      <img src="assets/imgs/dummy-user-img.png">\n      <div>\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n        <p style="font-size: 12px">\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n        <p style="font-size: 12px">\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n      </div>\n    </div> -->\n    <!-- </ion-navbar> -->\n  </ion-header>\n\n  <ion-content id=outerNew>\n    <div class="headProf">\n      <img src="assets/imgs/dummy-user-img.png">\n      <div>\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n        <p style="font-size: 12px">\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n        <p style="font-size: 12px">\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n      </div>\n    </div>\n    <!-- side menu content -->\n    <side-menu-content [settings]="sideMenuSettings" [options]="options" (change)="onOptionSelected($event)"></side-menu-content>\n    <!-- <ion-list no-lines>\n      <ion-item *ngFor="let p of pages;  let i=index" (click)="openPage(p, i);" style="background-color: transparent; border-bottom: 1px solid #717775; color: #f0f0f0">\n\n        <span ion-text>\n          <ion-icon name="{{p.icon}}"> </ion-icon>\n          &nbsp;&nbsp;&nbsp;{{p.title}}\n          <ion-icon [name]="selectedMenu == i? \'arrow-dropdown-circle\' : \'arrow-dropright-circle\'" *ngIf="p.subPages" float-right></ion-icon>\n        </span>\n\n        <ion-list no-lines [hidden]="selectedMenu != i" style="margin: 0px 0 0px; padding:0px !important;">\n          <ion-item no-border *ngFor="let subPage of p.subPages;let i2=index" text-wrap (click)="openPage(subPage)" style="background-color: transparent;border-bottom: 1px solid #b9c2bf; color: #f0f0f0; padding-left: 40px;font-size: 0.8em;">\n            <span ion-text>&nbsp;{{subPage.title}}</span>\n          </ion-item>\n        </ion-list>\n\n      </ion-item>\n    </ion-list> -->\n  </ion-content>\n  <!-- <ion-footer>\n    <ion-toolbar color="light" (tap)="logout()" class="toolbarmd">\n      <ion-title>\n        <ion-icon color="danger" name="log-out"></ion-icon>&nbsp;&nbsp;&nbsp;\n        <span ion-text color="danger">Logout</span>\n      </ion-title>\n    </ion-toolbar>\n  </ion-footer> -->\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__["a" /* MenuProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateGroup = /** @class */ (function () {
    function UpdateGroup(navCtrl, navParams, apigroupupdatecall, alertCtrl, modalCtrl, formBuilder, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupupdatecall = apigroupupdatecall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active"
            },
            {
                name: "InActive"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            grouptype: ['']
        });
    }
    UpdateGroup.prototype.ngOnInit = function () {
    };
    UpdateGroup.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-model',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\groups\update-group\update-group.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>Update Group</ion-title>\n\n            <ion-buttons end>\n\n                <button ion-button icon-only (click)="dismiss()">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n        </ion-navbar>\n\n    </ion-header>\n\n    <ion-content>\n\n     \n\n        <form [formGroup]="groupForm">\n\n\n\n            <ion-item>\n\n                    <ion-label fixed fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n                    <ion-input formControlName="group_name" type="text"></ion-input>\n\n           </ion-item>\n\n           \n\n\n\n           <ion-item>\n\n                <ion-label >Group Status*</ion-label>\n\n                <ion-select formControlName="status" style="min-width:50%;">\n\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n    \n\n      \n\n    </form>\n\n    <!-- <button ion-button block (click)="AddGroup()">ADD</button> -->\n\n        \n\n    </ion-content>\n\n    <ion-footer class="footSty">\n\n    \n\n            <ion-toolbar>\n\n                <ion-row no-padding>\n\n                    <ion-col width-50 style="text-align: center;">\n\n                        <button ion-button clear color="light" (click)="UpdateGroup()">UPDATE</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-toolbar>\n\n        </ion-footer>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\groups\update-group\update-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], UpdateGroup);
    return UpdateGroup;
}());

//# sourceMappingURL=update-group.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        //https://www.oneqlik.in
        this.usersURL = "https://www.oneqlik.in/users/";
        this.devicesURL = "https://www.oneqlik.in/devices";
        this.gpsURL = "https://www.oneqlik.in/gps";
        this.geofencingURL = "https://www.oneqlik.in/geofencing";
        this.trackRouteURL = "https://www.oneqlik.in/trackRoute";
        this.groupURL = "https://www.oneqlik.in/groups/";
        this.notifsURL = "https://www.oneqlik.in/notifs";
        this.stoppageURL = "https://www.oneqlik.in/stoppage";
        this.summaryURL = "https://www.oneqlik.in/summary";
        this.shareURL = "https://www.oneqlik.in/share";
        this.appId = "OneQlikVTS";
        console.log('Hello ApiServiceProvider Provider');
    }
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.startLoadingnew = function (key) {
        var str;
        if (key == 1) {
            str = 'unlocking';
        }
        else {
            str = 'locking';
        }
        return this.loading = this.loadingCtrl.create({
            content: "Please wait for some time, as we are " + str + " your vehicle...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.updateDL = function (updateDL) {
        return this.http.post('https://www.oneqlik.in/users' + "/zogoUserUpdate", updateDL, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSOSReport = function (url) {
        // return this.http.get("http://192.168.1.20:3000/notifs/SOSReport?from_date=" + starttime + '&to_date=' + endtime + '&dev_id=' + sos_id + '&_u=' + _id, { headers: this.headers })
        return this.http.get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.DealerSearchService = function (_id, pageno, limit, key) {
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDealerCall = function (deletePayload) {
        return this.http.post(this.usersURL + 'deleteUser', deletePayload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDealersCall = function (_id, pageno, limit) {
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.releaseAmount = function (uid) {
        return this.http.post('https://www.oneqlik.in/paytm/releaseAmount', uid)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateTripStatus = function (uId) {
        return this.http.get('https://www.oneqlik.in/zogo/updateZogoPaymentStatus?user=' + uId)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.proceedSecurely = function (withdrawObj) {
        return this.http.post('https://www.oneqlik.in/paytm/Withdraw', withdrawObj)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getblockamt = function () {
        return this.http.get('https://www.oneqlik.in/zogo/getInnitialBlockAmount')
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmbalance = function (userCred) {
        return this.http.get('https://www.oneqlik.in/paytm/checkBalance?CUST_ID=' + userCred + "&app_id=" + this.appId)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addMoneyPaytm = function (amtObj) {
        return this.http.post('https://www.oneqlik.in/paytm/addMoney', amtObj)
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.paytmLoginWallet = function (walletcredentials) {
        return this.http.post('https://www.oneqlik.in/paytm/sendOTP', walletcredentials)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmOTPvalidation = function (otpObj) {
        return this.http.post('https://www.oneqlik.in/paytm/validateOTP', otpObj)
            .map(function (res) { return res.json(); });
    };
    // callSearchService(email, id, key) {
    //   return this.http.get(this.devicesURL + "/getDeviceByUser?email=" + email + "&id=" + id + "&skip=0&limit=10&search=" + key, { headers: this.headers })
    //     .map(resp => resp.json());
    // }
    ApiServiceProvider.prototype.callSearchService = function (baseURL) {
        return this.http.get(baseURL, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.callResponse = function (_id) {
        return this.http.get("https://www.oneqlik.in/trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.serverLevelonoff = function (data) {
        return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePassword = function (data) {
        return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateprofile = function (data) {
        return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.get7daysData = function (a, t) {
        return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dataRemoveFuncCall = function (_id, did) {
        return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.tripReviewCall = function (device_id, stime, etime) {
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.sendTokenCall = function (payLoad) {
        return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.shareLivetrackCall = function (data) {
        return this.http.post(this.shareURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get("https://www.oneqlik.in/driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        // console.log("from date => "+ dates.fromDate.toISOString())
        // console.log("new date "+ new Date(dates.fromDate).toISOString())
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // getVehicleListCall(_id, email) {
    //   return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.getVehicleListCall = function (_url) {
        return this.http.get(_url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (_id, starttime, endtime, did) {
        return this.http.get('https://www.oneqlik.in/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        return this.http.post(this.trackRouteURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (_id, data) {
        return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (_id) {
        return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getStoppageApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getIgiApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getOverSpeedApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeogenceReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFuelApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceReportApi = function (starttime, endtime, _id, Ignitiondevice_id) {
        return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.contactusApi = function (contactdata) {
        return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllNotificationCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addgeofenceCall = function (data) {
        return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getdevicegeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofencestatusCall = function (_id, status, entering, exiting) {
        return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGeoCall = function (_id) {
        return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getallgeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getallgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.user_statusCall = function (data) {
        return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.editUserDetailsCall = function (devicedetails) {
        return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerVehiclesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addGroupCall = function (devicedetails) {
        return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleTypesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllUsersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDeviceModelCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.groupsCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addDeviceCall = function (devicedetails) {
        return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassApi = function (mobno) {
        return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassMobApi = function (Passwordset) {
        return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // dashboardcall(email, from, to, _id) {
    //   return this.http.get(this.gpsURL + '/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.dashboardcall = function (_baseUrl) {
        return this.http.get(_baseUrl, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppedDevices = function (_id, email, off_ids) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.livedatacall = function (_id, email) {
        return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesApi = function (_id, email) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesForAllVehiclesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.ignitionoffCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateCall = function (devicedetail) {
        return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gpsCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getcustToken = function (id) {
        return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSummaryReportApi = function (starttime, endtime, _id, device_id) {
        return this.http.get(this.summaryURL + "?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallrouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSpeedReport = function (_id, time) {
        return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // deviceupdateInCall(devicedetail) {
    //   return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.deleteDeviceCall = function (d_id) {
        return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.deviceShareCall = function (data) {
        return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pullnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGroupCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGroupCall = function (d_id) {
        return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // addcustomerCall(devicedetails) {
    //   return this.http.post(this.usersURL + 'signUp', devicedetails, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.deleteCustomerCall = function (data) {
        return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.route_details = function (_id, user_id) {
        return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callcustomerSearchService = function (uid, pageno, limit, seachKey) {
        return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ })

},[261]);
//# sourceMappingURL=main.js.map