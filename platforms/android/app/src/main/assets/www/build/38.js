webpackJsonp([38],{

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofencePageModule", function() { return GeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence__ = __webpack_require__(953);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GeofencePageModule = /** @class */ (function () {
    function GeofencePageModule() {
    }
    GeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */]),
            ],
        })
    ], GeofencePageModule);
    return GeofencePageModule;
}());

//# sourceMappingURL=geofence.module.js.map

/***/ }),

/***/ 953:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeofencePage = /** @class */ (function () {
    function GeofencePage(navCtrl, navParams, apiCall, toastCtrl, alerCtrl, modalCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log(this.islogin._id);
        // var login = this.islogin
        this.setsmsforotp = localStorage.getItem('setsms');
        this.isdevice = localStorage.getItem('cordinates');
        console.log("isdevice=> " + this.isdevice);
        console.log("setsmsforotp=> " + this.setsmsforotp);
        this.events.subscribe('reloadDetails', function () {
            //call methods to refresh content
            _this.apiCall.getallgeofenceCall(_this.islogin._id)
                .subscribe(function (data) {
                _this.devices1243 = [];
                _this.devices = data;
                console.log("devices=> ", _this.devices);
                localStorage.setItem('devices', _this.devices);
                _this.isdevice = localStorage.getItem('devices');
                console.log("isdevices=> ", _this.isdevice);
            }, function (err) {
                console.log("error => ", err);
            });
        });
    }
    GeofencePage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                // todo something
                // this.navController.pop();
                console.log("back button poped");
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        this.getgeofence();
    };
    GeofencePage.prototype.addgeofence = function () {
        this.navCtrl.push('AddGeofencePage');
    };
    GeofencePage.prototype.getgeofence = function () {
        var _this = this;
        console.log("getgeofence shape");
        // var baseURLp = 'http://13.126.36.205:3000/geofencing/getallgeofence?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.getallgeofenceCall(this.islogin._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            console.log("devices=> ", _this.devices);
            localStorage.setItem('devices', _this.devices);
            _this.isdevice = localStorage.getItem('devices');
            console.log("isdevices=> ", _this.isdevice);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
        });
    };
    GeofencePage.prototype.deleteGeo = function (_id) {
        var _this = this;
        // this.apiCall.startLoading().present();
        this.apiCall.deleteGeoCall(_id).
            subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.DeletedDevice = data;
            var toast = _this.toastCtrl.create({
                message: 'Deleted Geofence Area successfully.',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getgeofence();
            });
            toast.present();
        }, function (err) {
            // this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alerCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    GeofencePage.prototype.DelateGeofence = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: 'Do you want to delete this geofence area?',
            buttons: [{
                    text: 'No'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteGeo(_id);
                    }
                }]
        });
        alert.present();
    };
    GeofencePage.prototype.DisplayDataOnMap = function (item) {
        var _this = this;
        // console.log(item);
        // var baseURLp = 'http://13.126.36.205:3000/geofencing/geofencestatus?gid=' + item._id + '&status=' + item.status + '&entering=' + item.entering + '&exiting=' + item.exiting;
        this.apiCall.geofencestatusCall(item._id, item.status, item.entering, item.exiting)
            .subscribe(function (data) {
            _this.statusofgeofence = data;
            // console.log(this.statusofgeofence);
        }, function (err) {
            console.log(err);
        });
    };
    GeofencePage.prototype.geofenceShow = function (item) {
        this.navCtrl.push('GeofenceShowPage', {
            param: item
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], GeofencePage.prototype, "navBar", void 0);
    GeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\geofence\geofence.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Geofences</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (tap)="addgeofence()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let item of devices">\n\n    <ion-item (click)="geofenceShow(item)">\n\n      <h2>{{item.geoname}}</h2>\n\n      <p style="margin-top: 3%;">Number Of Vehicles - {{item.devicesWithin.length}}</p>\n\n      <ion-icon item-end name="trash" color="danger" (tap)="DelateGeofence(item._id)"> </ion-icon>\n\n    </ion-item>\n\n    <!-- <ion-toggle color="danger"></ion-toggle> -->\n\n    <ion-row>\n\n      <ion-col width-33>\n\n        <div style="padding-left: 10px;">\n\n          <ion-toggle [(ngModel)]="item.status" (ionChange)="DisplayDataOnMap(item)" color="danger"></ion-toggle>\n\n        </div>\n\n\n\n        <!-- <ion-toggle [(ngModel)]="item.status" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left:11%;"></ion-toggle> -->\n\n        <p style="color:#e618af;font-size:10px;margin-left: 10px;">Display on map</p>\n\n      </ion-col>\n\n      <ion-col width-33 style="text-align: center;">\n\n        <div style="padding-left: 10px;">\n\n          <ion-toggle [(ngModel)]="item.entering" (ionChange)="DisplayDataOnMap(item)" color="danger"></ion-toggle>\n\n        </div>\n\n        <!-- <ion-toggle [(ngModel)]="item.entering" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n\n        <p style="color:green;font-size: 10px;margin-left: -8%;">Notification Entering</p>\n\n      </ion-col>\n\n      <ion-col width-33>\n\n        <div style="padding-left: 10px;">\n\n          <ion-toggle [(ngModel)]="item.exiting" (ionChange)="DisplayDataOnMap(item)" color="danger"></ion-toggle>\n\n        </div>\n\n        <!-- <ion-toggle [(ngModel)]="item.exiting" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n\n        <p style="color:green;font-size: 10px; margin-left: 11%;">Notification Exiting</p>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n  </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\geofence\geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], GeofencePage);
    return GeofencePage;
}());

//# sourceMappingURL=geofence.js.map

/***/ })

});
//# sourceMappingURL=38.js.map