webpackJsonp([35],{

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpiredPageModule", function() { return ExpiredPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expired__ = __webpack_require__(957);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ExpiredPageModule = /** @class */ (function () {
    function ExpiredPageModule() {
    }
    ExpiredPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expired__["a" /* ExpiredPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__expired__["a" /* ExpiredPage */]),
            ],
            providers: [],
        })
    ], ExpiredPageModule);
    return ExpiredPageModule;
}());

//# sourceMappingURL=expired.module.js.map

/***/ }),

/***/ 957:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpiredPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExpiredPage = /** @class */ (function () {
    function ExpiredPage(params, viewCtrl, toastCtrl, navCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
    }
    ExpiredPage.prototype.activate = function () {
        // this.navCtrl.push("AddDevicesPage");
        console.log("Redirect to paytm page");
    };
    ExpiredPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ExpiredPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-expired',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\live\expired\expired.html"*/'<ion-content padding class="guide-modal">\n\n\n\n    <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n    <div>\n\n        <p style="text-align: center; font-size: 2rem">\n\n            <b>Device Expired!</b>\n\n        </p>\n\n        <p>\n\n            <button ion-button block outline color="secondary" (tap)="activate()">Activate</button>\n\n        </p>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\live\expired\expired.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], ExpiredPage);
    return ExpiredPage;
}());

//# sourceMappingURL=expired.js.map

/***/ })

});
//# sourceMappingURL=35.js.map