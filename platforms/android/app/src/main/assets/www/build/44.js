webpackJsonp([44],{

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function() { return ContactUsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us__ = __webpack_require__(939);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContactUsPageModule = /** @class */ (function () {
    function ContactUsPageModule() {
    }
    ContactUsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"]
            ],
        })
    ], ContactUsPageModule);
    return ContactUsPageModule;
}());

//# sourceMappingURL=contact-us.module.js.map

/***/ }),

/***/ 939:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactUsPage = /** @class */ (function () {
    function ContactUsPage(navCtrl, navParams, formBuilder, api, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.contact_data = {};
        this.contactusForm = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            mail: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            mobno: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            // title: ['', Validators.required],
            note: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    ContactUsPage_1 = ContactUsPage;
    ContactUsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactUsPage');
    };
    ContactUsPage.prototype.contactUs = function () {
        var _this = this;
        this.submitAttempt = true;
        console.log(this.contactusForm.value);
        if (this.contactusForm.valid) {
            this.contact_data = {
                "email": this.contactusForm.value.mail,
                // "title": this.contactusForm.value.title,
                "msg": this.contactusForm.value.note,
                "phone": this.contactusForm.value.mobno,
                "dealerid": "gaurav.gupta@adnatesolutions.com",
                "dealerName": "Gaurav Gupta"
            };
            this.api.startLoading().present();
            this.api.contactusApi(this.contact_data)
                .subscribe(function (data) {
                _this.api.stopLoading();
                console.log(data.message);
                var toast = _this.toastCtrl.create({
                    message: 'Your message has been sent, we will get back to you soon..',
                    position: 'bottom',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.navCtrl.setRoot(ContactUsPage_1);
                });
                toast.present();
            }, function (error) {
                _this.api.stopLoading();
                console.log(error);
            });
        }
    };
    ContactUsPage = ContactUsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-contact-us',template:/*ion-inline-start:"D:\Pro\ionic-skystargps\src\pages\contact-us\contact-us.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Contact Us</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <!-- <ion-card>\n        <ion-card-header style="background: brown; color: white;">\n          Customer support contacts\n        </ion-card-header>\n        <ion-card-content padding-top>\n    \n          <ion-icon ios="ios-call" md="md-call" style="color:#33cd5f;" (click)="dialSupportNumber()"></ion-icon>\n          &nbsp;&nbsp;\n          <a href="tel:+91-9028045389">\n            <span ion-text style="font-size: 16px;">+91 9028045389</span>\n          </a>\n          <br/>\n    \n          <ion-icon ios="ios-mail" md="md-mail" style="color:red;" (click)="dialSupportNumber()"></ion-icon>&nbsp;&nbsp;\n          <a href="mailto:poonam.g@processfactory.in">\n            <span ion-text style="font-size: 16px;">poonam.g@processfactory.in</span>\n          </a>\n        </ion-card-content>\n      </ion-card> -->\n\n  <form class="form" [formGroup]="contactusForm">\n    <p type="Name:">\n      <input formControlName="name" type="text" placeholder="Write your name here.." />\n    </p>\n    <span class="span" *ngIf="!contactusForm.controls.name.valid && (contactusForm.controls.name.dirty || submitAttempt)">Name is required and should be in valid format!</span>\n    <p type="Email:">\n      <input formControlName="mail" type="email" placeholder="Let us know how to contact you back.." />\n    </p>\n    <span class="span" *ngIf="!contactusForm.controls.mail.valid && (contactusForm.controls.mail.dirty || submitAttempt)">Email id is required and should be in valid format!</span>\n    <p type="Mobile Num.:">\n      <input formControlName="mobno" type="number" maxlength="10" minlength="10" placeholder="Let us know how to contact you back via mobile number.." />\n    </p>\n    <span class="span" *ngIf="!contactusForm.controls.mobno.valid && (contactusForm.controls.mobno.dirty || submitAttempt)">Mobile number is required and should be in 10 digits format!</span>\n    <p type="Message:">\n      <textarea rows="2" cols="50" formControlName="note" placeholder="What would you like to tell us.."></textarea>\n      <!-- <textarea cols="4" formControlName="note" type="text" placeholder="What would you like to tell us.." /> -->\n    </p>\n    <span class="span" *ngIf="!contactusForm.controls.note.valid && (contactusForm.controls.note.dirty || submitAttempt)">please write your message!</span>\n    <button (tap)="contactUs()">Send Message</button>\n\n    <!-- <div class="div">\n      <ion-icon name="call"></ion-icon> +91-9028045389\n      <ion-icon name="mail"></ion-icon> support@oneqlik.in\n      <ion-icon name="call"></ion-icon>\n      <a href="tel:+91-9028045389" style="color: white">+91-9028045389</a>\n      <ion-icon name="mail"></ion-icon>\n      <a href="mailto:poonam.g@processfactory.in" style="color: white">poonam.g@processfactory.in</a>\n    </div> -->\n  </form>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\ionic-skystargps\src\pages\contact-us\contact-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ContactUsPage);
    return ContactUsPage;
    var ContactUsPage_1;
}());

//# sourceMappingURL=contact-us.js.map

/***/ })

});
//# sourceMappingURL=44.js.map